﻿namespace ExtTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.enemy1 = new System.Windows.Forms.TrackBar();
            this.enemy2 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.loadButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.value1 = new System.Windows.Forms.Label();
            this.value2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkRanged = new System.Windows.Forms.CheckBox();
            this.checkMelee = new System.Windows.Forms.CheckBox();
            this.pB1 = new System.Windows.Forms.PictureBox();
            this.pB2 = new System.Windows.Forms.PictureBox();
            this.pB3 = new System.Windows.Forms.PictureBox();
            this.pB4 = new System.Windows.Forms.PictureBox();
            this.pB5 = new System.Windows.Forms.PictureBox();
            this.pB6 = new System.Windows.Forms.PictureBox();
            this.pB7 = new System.Windows.Forms.PictureBox();
            this.pB8 = new System.Windows.Forms.PictureBox();
            this.pB9 = new System.Windows.Forms.PictureBox();
            this.pB10 = new System.Windows.Forms.PictureBox();
            this.pB11 = new System.Windows.Forms.PictureBox();
            this.pB12 = new System.Windows.Forms.PictureBox();
            this.pB13 = new System.Windows.Forms.PictureBox();
            this.pB14 = new System.Windows.Forms.PictureBox();
            this.pB15 = new System.Windows.Forms.PictureBox();
            this.pB16 = new System.Windows.Forms.PictureBox();
            this.pB17 = new System.Windows.Forms.PictureBox();
            this.pB18 = new System.Windows.Forms.PictureBox();
            this.pB19 = new System.Windows.Forms.PictureBox();
            this.pB20 = new System.Windows.Forms.PictureBox();
            this.pB21 = new System.Windows.Forms.PictureBox();
            this.pB22 = new System.Windows.Forms.PictureBox();
            this.pB23 = new System.Windows.Forms.PictureBox();
            this.pB24 = new System.Windows.Forms.PictureBox();
            this.pB25 = new System.Windows.Forms.PictureBox();
            this.pB26 = new System.Windows.Forms.PictureBox();
            this.pB27 = new System.Windows.Forms.PictureBox();
            this.pB28 = new System.Windows.Forms.PictureBox();
            this.pB29 = new System.Windows.Forms.PictureBox();
            this.pB30 = new System.Windows.Forms.PictureBox();
            this.pB31 = new System.Windows.Forms.PictureBox();
            this.pB32 = new System.Windows.Forms.PictureBox();
            this.pB33 = new System.Windows.Forms.PictureBox();
            this.pB34 = new System.Windows.Forms.PictureBox();
            this.pB35 = new System.Windows.Forms.PictureBox();
            this.pB36 = new System.Windows.Forms.PictureBox();
            this.pB37 = new System.Windows.Forms.PictureBox();
            this.pB38 = new System.Windows.Forms.PictureBox();
            this.pB39 = new System.Windows.Forms.PictureBox();
            this.pB40 = new System.Windows.Forms.PictureBox();
            this.pB41 = new System.Windows.Forms.PictureBox();
            this.pB42 = new System.Windows.Forms.PictureBox();
            this.pB43 = new System.Windows.Forms.PictureBox();
            this.pB44 = new System.Windows.Forms.PictureBox();
            this.pB45 = new System.Windows.Forms.PictureBox();
            this.pB46 = new System.Windows.Forms.PictureBox();
            this.pB47 = new System.Windows.Forms.PictureBox();
            this.pB48 = new System.Windows.Forms.PictureBox();
            this.pB49 = new System.Windows.Forms.PictureBox();
            this.pB50 = new System.Windows.Forms.PictureBox();
            this.pB51 = new System.Windows.Forms.PictureBox();
            this.pB52 = new System.Windows.Forms.PictureBox();
            this.pB53 = new System.Windows.Forms.PictureBox();
            this.pB54 = new System.Windows.Forms.PictureBox();
            this.pB55 = new System.Windows.Forms.PictureBox();
            this.pB56 = new System.Windows.Forms.PictureBox();
            this.pB57 = new System.Windows.Forms.PictureBox();
            this.pB58 = new System.Windows.Forms.PictureBox();
            this.pB59 = new System.Windows.Forms.PictureBox();
            this.pB60 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.value4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.enemy4 = new System.Windows.Forms.TrackBar();
            this.value3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.enemy3 = new System.Windows.Forms.TrackBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.enemy1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB60)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enemy4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy3)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // enemy1
            // 
            this.enemy1.Location = new System.Drawing.Point(177, 0);
            this.enemy1.Name = "enemy1";
            this.enemy1.Size = new System.Drawing.Size(189, 45);
            this.enemy1.TabIndex = 1;
            this.enemy1.Scroll += new System.EventHandler(this.enemy1_Scroll);
            // 
            // enemy2
            // 
            this.enemy2.Location = new System.Drawing.Point(177, 45);
            this.enemy2.Name = "enemy2";
            this.enemy2.Size = new System.Drawing.Size(189, 45);
            this.enemy2.TabIndex = 2;
            this.enemy2.Scroll += new System.EventHandler(this.enemy2_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Standard";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Jumping";
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(284, 562);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(111, 33);
            this.loadButton.TabIndex = 7;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(496, 562);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(102, 33);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // value1
            // 
            this.value1.AutoSize = true;
            this.value1.Location = new System.Drawing.Point(162, 10);
            this.value1.Name = "value1";
            this.value1.Size = new System.Drawing.Size(13, 13);
            this.value1.TabIndex = 9;
            this.value1.Text = "0";
            // 
            // value2
            // 
            this.value2.AutoSize = true;
            this.value2.Location = new System.Drawing.Point(158, 53);
            this.value2.Name = "value2";
            this.value2.Size = new System.Drawing.Size(13, 13);
            this.value2.TabIndex = 10;
            this.value2.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 48);
            this.label3.TabIndex = 11;
            this.label3.Text = "Enemy Types:\r\n(Amount to \r\nspawn)\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Character Options:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkRanged);
            this.groupBox1.Controls.Add(this.checkMelee);
            this.groupBox1.Location = new System.Drawing.Point(3, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 56);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Attack Type";
            // 
            // checkRanged
            // 
            this.checkRanged.AutoSize = true;
            this.checkRanged.Location = new System.Drawing.Point(129, 23);
            this.checkRanged.Name = "checkRanged";
            this.checkRanged.Size = new System.Drawing.Size(64, 17);
            this.checkRanged.TabIndex = 1;
            this.checkRanged.Text = "Ranged";
            this.checkRanged.UseVisualStyleBackColor = true;
            // 
            // checkMelee
            // 
            this.checkMelee.AutoSize = true;
            this.checkMelee.Location = new System.Drawing.Point(6, 23);
            this.checkMelee.Name = "checkMelee";
            this.checkMelee.Size = new System.Drawing.Size(55, 17);
            this.checkMelee.TabIndex = 0;
            this.checkMelee.Text = "Melee";
            this.checkMelee.UseVisualStyleBackColor = true;
            // 
            // pB1
            // 
            this.pB1.BackColor = System.Drawing.SystemColors.Window;
            this.pB1.Location = new System.Drawing.Point(29, 35);
            this.pB1.Name = "pB1";
            this.pB1.Size = new System.Drawing.Size(74, 59);
            this.pB1.TabIndex = 14;
            this.pB1.TabStop = false;
            this.pB1.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB2
            // 
            this.pB2.BackColor = System.Drawing.SystemColors.Window;
            this.pB2.Location = new System.Drawing.Point(109, 35);
            this.pB2.Name = "pB2";
            this.pB2.Size = new System.Drawing.Size(74, 59);
            this.pB2.TabIndex = 15;
            this.pB2.TabStop = false;
            this.pB2.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB3
            // 
            this.pB3.BackColor = System.Drawing.SystemColors.Window;
            this.pB3.Location = new System.Drawing.Point(189, 35);
            this.pB3.Name = "pB3";
            this.pB3.Size = new System.Drawing.Size(74, 59);
            this.pB3.TabIndex = 16;
            this.pB3.TabStop = false;
            this.pB3.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB4
            // 
            this.pB4.BackColor = System.Drawing.SystemColors.Window;
            this.pB4.Location = new System.Drawing.Point(269, 35);
            this.pB4.Name = "pB4";
            this.pB4.Size = new System.Drawing.Size(74, 59);
            this.pB4.TabIndex = 17;
            this.pB4.TabStop = false;
            this.pB4.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB5
            // 
            this.pB5.BackColor = System.Drawing.SystemColors.Window;
            this.pB5.Location = new System.Drawing.Point(349, 35);
            this.pB5.Name = "pB5";
            this.pB5.Size = new System.Drawing.Size(74, 59);
            this.pB5.TabIndex = 18;
            this.pB5.TabStop = false;
            this.pB5.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB6
            // 
            this.pB6.BackColor = System.Drawing.SystemColors.Window;
            this.pB6.Location = new System.Drawing.Point(429, 35);
            this.pB6.Name = "pB6";
            this.pB6.Size = new System.Drawing.Size(74, 59);
            this.pB6.TabIndex = 19;
            this.pB6.TabStop = false;
            this.pB6.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB7
            // 
            this.pB7.BackColor = System.Drawing.SystemColors.Window;
            this.pB7.Location = new System.Drawing.Point(509, 35);
            this.pB7.Name = "pB7";
            this.pB7.Size = new System.Drawing.Size(74, 59);
            this.pB7.TabIndex = 20;
            this.pB7.TabStop = false;
            this.pB7.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB8
            // 
            this.pB8.BackColor = System.Drawing.SystemColors.Window;
            this.pB8.Location = new System.Drawing.Point(589, 35);
            this.pB8.Name = "pB8";
            this.pB8.Size = new System.Drawing.Size(74, 59);
            this.pB8.TabIndex = 21;
            this.pB8.TabStop = false;
            this.pB8.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB9
            // 
            this.pB9.BackColor = System.Drawing.SystemColors.Window;
            this.pB9.Location = new System.Drawing.Point(669, 35);
            this.pB9.Name = "pB9";
            this.pB9.Size = new System.Drawing.Size(74, 59);
            this.pB9.TabIndex = 22;
            this.pB9.TabStop = false;
            this.pB9.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB10
            // 
            this.pB10.BackColor = System.Drawing.SystemColors.Window;
            this.pB10.Location = new System.Drawing.Point(749, 35);
            this.pB10.Name = "pB10";
            this.pB10.Size = new System.Drawing.Size(74, 59);
            this.pB10.TabIndex = 23;
            this.pB10.TabStop = false;
            this.pB10.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB11
            // 
            this.pB11.BackColor = System.Drawing.SystemColors.Window;
            this.pB11.Location = new System.Drawing.Point(29, 100);
            this.pB11.Name = "pB11";
            this.pB11.Size = new System.Drawing.Size(74, 59);
            this.pB11.TabIndex = 24;
            this.pB11.TabStop = false;
            this.pB11.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB12
            // 
            this.pB12.BackColor = System.Drawing.SystemColors.Window;
            this.pB12.Location = new System.Drawing.Point(109, 100);
            this.pB12.Name = "pB12";
            this.pB12.Size = new System.Drawing.Size(74, 59);
            this.pB12.TabIndex = 25;
            this.pB12.TabStop = false;
            this.pB12.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB13
            // 
            this.pB13.BackColor = System.Drawing.SystemColors.Window;
            this.pB13.Location = new System.Drawing.Point(189, 100);
            this.pB13.Name = "pB13";
            this.pB13.Size = new System.Drawing.Size(74, 59);
            this.pB13.TabIndex = 26;
            this.pB13.TabStop = false;
            this.pB13.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB14
            // 
            this.pB14.BackColor = System.Drawing.SystemColors.Window;
            this.pB14.Location = new System.Drawing.Point(269, 100);
            this.pB14.Name = "pB14";
            this.pB14.Size = new System.Drawing.Size(74, 59);
            this.pB14.TabIndex = 27;
            this.pB14.TabStop = false;
            this.pB14.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB15
            // 
            this.pB15.BackColor = System.Drawing.SystemColors.Window;
            this.pB15.Location = new System.Drawing.Point(349, 100);
            this.pB15.Name = "pB15";
            this.pB15.Size = new System.Drawing.Size(74, 59);
            this.pB15.TabIndex = 28;
            this.pB15.TabStop = false;
            this.pB15.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB16
            // 
            this.pB16.BackColor = System.Drawing.SystemColors.Window;
            this.pB16.Location = new System.Drawing.Point(429, 100);
            this.pB16.Name = "pB16";
            this.pB16.Size = new System.Drawing.Size(74, 59);
            this.pB16.TabIndex = 29;
            this.pB16.TabStop = false;
            this.pB16.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB17
            // 
            this.pB17.BackColor = System.Drawing.SystemColors.Window;
            this.pB17.Location = new System.Drawing.Point(509, 100);
            this.pB17.Name = "pB17";
            this.pB17.Size = new System.Drawing.Size(74, 59);
            this.pB17.TabIndex = 30;
            this.pB17.TabStop = false;
            this.pB17.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB18
            // 
            this.pB18.BackColor = System.Drawing.SystemColors.Window;
            this.pB18.Location = new System.Drawing.Point(589, 100);
            this.pB18.Name = "pB18";
            this.pB18.Size = new System.Drawing.Size(74, 59);
            this.pB18.TabIndex = 31;
            this.pB18.TabStop = false;
            this.pB18.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB19
            // 
            this.pB19.BackColor = System.Drawing.SystemColors.Window;
            this.pB19.Location = new System.Drawing.Point(669, 100);
            this.pB19.Name = "pB19";
            this.pB19.Size = new System.Drawing.Size(74, 59);
            this.pB19.TabIndex = 32;
            this.pB19.TabStop = false;
            this.pB19.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB20
            // 
            this.pB20.BackColor = System.Drawing.SystemColors.Window;
            this.pB20.Location = new System.Drawing.Point(749, 100);
            this.pB20.Name = "pB20";
            this.pB20.Size = new System.Drawing.Size(74, 59);
            this.pB20.TabIndex = 33;
            this.pB20.TabStop = false;
            this.pB20.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB21
            // 
            this.pB21.BackColor = System.Drawing.SystemColors.Window;
            this.pB21.Location = new System.Drawing.Point(29, 165);
            this.pB21.Name = "pB21";
            this.pB21.Size = new System.Drawing.Size(74, 59);
            this.pB21.TabIndex = 34;
            this.pB21.TabStop = false;
            this.pB21.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB22
            // 
            this.pB22.BackColor = System.Drawing.SystemColors.Window;
            this.pB22.Location = new System.Drawing.Point(109, 165);
            this.pB22.Name = "pB22";
            this.pB22.Size = new System.Drawing.Size(74, 59);
            this.pB22.TabIndex = 35;
            this.pB22.TabStop = false;
            this.pB22.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB23
            // 
            this.pB23.BackColor = System.Drawing.SystemColors.Window;
            this.pB23.Location = new System.Drawing.Point(189, 165);
            this.pB23.Name = "pB23";
            this.pB23.Size = new System.Drawing.Size(74, 59);
            this.pB23.TabIndex = 36;
            this.pB23.TabStop = false;
            this.pB23.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB24
            // 
            this.pB24.BackColor = System.Drawing.SystemColors.Window;
            this.pB24.Location = new System.Drawing.Point(269, 165);
            this.pB24.Name = "pB24";
            this.pB24.Size = new System.Drawing.Size(74, 59);
            this.pB24.TabIndex = 37;
            this.pB24.TabStop = false;
            this.pB24.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB25
            // 
            this.pB25.BackColor = System.Drawing.SystemColors.Window;
            this.pB25.Location = new System.Drawing.Point(349, 165);
            this.pB25.Name = "pB25";
            this.pB25.Size = new System.Drawing.Size(74, 59);
            this.pB25.TabIndex = 38;
            this.pB25.TabStop = false;
            this.pB25.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB26
            // 
            this.pB26.BackColor = System.Drawing.SystemColors.Window;
            this.pB26.Location = new System.Drawing.Point(429, 165);
            this.pB26.Name = "pB26";
            this.pB26.Size = new System.Drawing.Size(74, 59);
            this.pB26.TabIndex = 39;
            this.pB26.TabStop = false;
            this.pB26.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB27
            // 
            this.pB27.BackColor = System.Drawing.SystemColors.Window;
            this.pB27.Location = new System.Drawing.Point(509, 165);
            this.pB27.Name = "pB27";
            this.pB27.Size = new System.Drawing.Size(74, 59);
            this.pB27.TabIndex = 40;
            this.pB27.TabStop = false;
            this.pB27.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB28
            // 
            this.pB28.BackColor = System.Drawing.SystemColors.Window;
            this.pB28.Location = new System.Drawing.Point(589, 165);
            this.pB28.Name = "pB28";
            this.pB28.Size = new System.Drawing.Size(74, 59);
            this.pB28.TabIndex = 41;
            this.pB28.TabStop = false;
            this.pB28.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB29
            // 
            this.pB29.BackColor = System.Drawing.SystemColors.Window;
            this.pB29.Location = new System.Drawing.Point(669, 165);
            this.pB29.Name = "pB29";
            this.pB29.Size = new System.Drawing.Size(74, 59);
            this.pB29.TabIndex = 42;
            this.pB29.TabStop = false;
            this.pB29.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB30
            // 
            this.pB30.BackColor = System.Drawing.SystemColors.Window;
            this.pB30.Location = new System.Drawing.Point(749, 165);
            this.pB30.Name = "pB30";
            this.pB30.Size = new System.Drawing.Size(74, 59);
            this.pB30.TabIndex = 43;
            this.pB30.TabStop = false;
            this.pB30.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB31
            // 
            this.pB31.BackColor = System.Drawing.SystemColors.Window;
            this.pB31.Location = new System.Drawing.Point(29, 230);
            this.pB31.Name = "pB31";
            this.pB31.Size = new System.Drawing.Size(74, 59);
            this.pB31.TabIndex = 44;
            this.pB31.TabStop = false;
            this.pB31.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB32
            // 
            this.pB32.BackColor = System.Drawing.SystemColors.Window;
            this.pB32.Location = new System.Drawing.Point(109, 230);
            this.pB32.Name = "pB32";
            this.pB32.Size = new System.Drawing.Size(74, 59);
            this.pB32.TabIndex = 45;
            this.pB32.TabStop = false;
            this.pB32.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB33
            // 
            this.pB33.BackColor = System.Drawing.SystemColors.Window;
            this.pB33.Location = new System.Drawing.Point(189, 230);
            this.pB33.Name = "pB33";
            this.pB33.Size = new System.Drawing.Size(74, 59);
            this.pB33.TabIndex = 46;
            this.pB33.TabStop = false;
            this.pB33.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB34
            // 
            this.pB34.BackColor = System.Drawing.SystemColors.Window;
            this.pB34.Location = new System.Drawing.Point(269, 230);
            this.pB34.Name = "pB34";
            this.pB34.Size = new System.Drawing.Size(74, 59);
            this.pB34.TabIndex = 47;
            this.pB34.TabStop = false;
            this.pB34.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB35
            // 
            this.pB35.BackColor = System.Drawing.SystemColors.Window;
            this.pB35.Location = new System.Drawing.Point(349, 230);
            this.pB35.Name = "pB35";
            this.pB35.Size = new System.Drawing.Size(74, 59);
            this.pB35.TabIndex = 48;
            this.pB35.TabStop = false;
            this.pB35.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB36
            // 
            this.pB36.BackColor = System.Drawing.SystemColors.Window;
            this.pB36.Location = new System.Drawing.Point(429, 230);
            this.pB36.Name = "pB36";
            this.pB36.Size = new System.Drawing.Size(74, 59);
            this.pB36.TabIndex = 49;
            this.pB36.TabStop = false;
            this.pB36.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB37
            // 
            this.pB37.BackColor = System.Drawing.SystemColors.Window;
            this.pB37.Location = new System.Drawing.Point(509, 230);
            this.pB37.Name = "pB37";
            this.pB37.Size = new System.Drawing.Size(74, 59);
            this.pB37.TabIndex = 50;
            this.pB37.TabStop = false;
            this.pB37.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB38
            // 
            this.pB38.BackColor = System.Drawing.SystemColors.Window;
            this.pB38.Location = new System.Drawing.Point(589, 230);
            this.pB38.Name = "pB38";
            this.pB38.Size = new System.Drawing.Size(74, 59);
            this.pB38.TabIndex = 51;
            this.pB38.TabStop = false;
            this.pB38.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB39
            // 
            this.pB39.BackColor = System.Drawing.SystemColors.Window;
            this.pB39.Location = new System.Drawing.Point(669, 230);
            this.pB39.Name = "pB39";
            this.pB39.Size = new System.Drawing.Size(74, 59);
            this.pB39.TabIndex = 52;
            this.pB39.TabStop = false;
            this.pB39.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB40
            // 
            this.pB40.BackColor = System.Drawing.SystemColors.Window;
            this.pB40.Location = new System.Drawing.Point(749, 230);
            this.pB40.Name = "pB40";
            this.pB40.Size = new System.Drawing.Size(74, 59);
            this.pB40.TabIndex = 53;
            this.pB40.TabStop = false;
            this.pB40.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB41
            // 
            this.pB41.BackColor = System.Drawing.SystemColors.Window;
            this.pB41.Location = new System.Drawing.Point(29, 295);
            this.pB41.Name = "pB41";
            this.pB41.Size = new System.Drawing.Size(74, 59);
            this.pB41.TabIndex = 54;
            this.pB41.TabStop = false;
            this.pB41.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB42
            // 
            this.pB42.BackColor = System.Drawing.SystemColors.Window;
            this.pB42.Location = new System.Drawing.Point(109, 295);
            this.pB42.Name = "pB42";
            this.pB42.Size = new System.Drawing.Size(74, 59);
            this.pB42.TabIndex = 55;
            this.pB42.TabStop = false;
            this.pB42.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB43
            // 
            this.pB43.BackColor = System.Drawing.SystemColors.Window;
            this.pB43.Location = new System.Drawing.Point(189, 295);
            this.pB43.Name = "pB43";
            this.pB43.Size = new System.Drawing.Size(74, 59);
            this.pB43.TabIndex = 56;
            this.pB43.TabStop = false;
            this.pB43.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB44
            // 
            this.pB44.BackColor = System.Drawing.SystemColors.Window;
            this.pB44.Location = new System.Drawing.Point(269, 295);
            this.pB44.Name = "pB44";
            this.pB44.Size = new System.Drawing.Size(74, 59);
            this.pB44.TabIndex = 57;
            this.pB44.TabStop = false;
            this.pB44.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB45
            // 
            this.pB45.BackColor = System.Drawing.SystemColors.Window;
            this.pB45.Location = new System.Drawing.Point(349, 295);
            this.pB45.Name = "pB45";
            this.pB45.Size = new System.Drawing.Size(74, 59);
            this.pB45.TabIndex = 58;
            this.pB45.TabStop = false;
            this.pB45.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB46
            // 
            this.pB46.BackColor = System.Drawing.SystemColors.Window;
            this.pB46.Location = new System.Drawing.Point(429, 295);
            this.pB46.Name = "pB46";
            this.pB46.Size = new System.Drawing.Size(74, 59);
            this.pB46.TabIndex = 59;
            this.pB46.TabStop = false;
            this.pB46.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB47
            // 
            this.pB47.BackColor = System.Drawing.SystemColors.Window;
            this.pB47.Location = new System.Drawing.Point(509, 295);
            this.pB47.Name = "pB47";
            this.pB47.Size = new System.Drawing.Size(74, 59);
            this.pB47.TabIndex = 60;
            this.pB47.TabStop = false;
            this.pB47.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB48
            // 
            this.pB48.BackColor = System.Drawing.SystemColors.Window;
            this.pB48.Location = new System.Drawing.Point(589, 295);
            this.pB48.Name = "pB48";
            this.pB48.Size = new System.Drawing.Size(74, 59);
            this.pB48.TabIndex = 61;
            this.pB48.TabStop = false;
            this.pB48.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB49
            // 
            this.pB49.BackColor = System.Drawing.SystemColors.Window;
            this.pB49.Location = new System.Drawing.Point(669, 295);
            this.pB49.Name = "pB49";
            this.pB49.Size = new System.Drawing.Size(74, 59);
            this.pB49.TabIndex = 62;
            this.pB49.TabStop = false;
            this.pB49.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB50
            // 
            this.pB50.BackColor = System.Drawing.SystemColors.Window;
            this.pB50.Location = new System.Drawing.Point(749, 295);
            this.pB50.Name = "pB50";
            this.pB50.Size = new System.Drawing.Size(74, 59);
            this.pB50.TabIndex = 63;
            this.pB50.TabStop = false;
            this.pB50.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB51
            // 
            this.pB51.BackColor = System.Drawing.SystemColors.Window;
            this.pB51.Location = new System.Drawing.Point(29, 360);
            this.pB51.Name = "pB51";
            this.pB51.Size = new System.Drawing.Size(74, 59);
            this.pB51.TabIndex = 64;
            this.pB51.TabStop = false;
            this.pB51.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB52
            // 
            this.pB52.BackColor = System.Drawing.SystemColors.Window;
            this.pB52.Location = new System.Drawing.Point(109, 360);
            this.pB52.Name = "pB52";
            this.pB52.Size = new System.Drawing.Size(74, 59);
            this.pB52.TabIndex = 65;
            this.pB52.TabStop = false;
            this.pB52.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB53
            // 
            this.pB53.BackColor = System.Drawing.SystemColors.Window;
            this.pB53.Location = new System.Drawing.Point(189, 360);
            this.pB53.Name = "pB53";
            this.pB53.Size = new System.Drawing.Size(74, 59);
            this.pB53.TabIndex = 66;
            this.pB53.TabStop = false;
            this.pB53.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB54
            // 
            this.pB54.BackColor = System.Drawing.SystemColors.Window;
            this.pB54.Location = new System.Drawing.Point(269, 360);
            this.pB54.Name = "pB54";
            this.pB54.Size = new System.Drawing.Size(74, 59);
            this.pB54.TabIndex = 67;
            this.pB54.TabStop = false;
            this.pB54.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB55
            // 
            this.pB55.BackColor = System.Drawing.SystemColors.Window;
            this.pB55.Location = new System.Drawing.Point(349, 360);
            this.pB55.Name = "pB55";
            this.pB55.Size = new System.Drawing.Size(74, 59);
            this.pB55.TabIndex = 68;
            this.pB55.TabStop = false;
            this.pB55.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB56
            // 
            this.pB56.BackColor = System.Drawing.SystemColors.Window;
            this.pB56.Location = new System.Drawing.Point(429, 360);
            this.pB56.Name = "pB56";
            this.pB56.Size = new System.Drawing.Size(74, 59);
            this.pB56.TabIndex = 69;
            this.pB56.TabStop = false;
            this.pB56.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB57
            // 
            this.pB57.BackColor = System.Drawing.SystemColors.Window;
            this.pB57.Location = new System.Drawing.Point(509, 360);
            this.pB57.Name = "pB57";
            this.pB57.Size = new System.Drawing.Size(74, 59);
            this.pB57.TabIndex = 70;
            this.pB57.TabStop = false;
            this.pB57.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB58
            // 
            this.pB58.BackColor = System.Drawing.SystemColors.Window;
            this.pB58.Location = new System.Drawing.Point(589, 360);
            this.pB58.Name = "pB58";
            this.pB58.Size = new System.Drawing.Size(74, 59);
            this.pB58.TabIndex = 71;
            this.pB58.TabStop = false;
            this.pB58.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB59
            // 
            this.pB59.BackColor = System.Drawing.SystemColors.Window;
            this.pB59.Location = new System.Drawing.Point(669, 360);
            this.pB59.Name = "pB59";
            this.pB59.Size = new System.Drawing.Size(74, 59);
            this.pB59.TabIndex = 72;
            this.pB59.TabStop = false;
            this.pB59.Click += new System.EventHandler(this.ChangeImage);
            // 
            // pB60
            // 
            this.pB60.BackColor = System.Drawing.SystemColors.Window;
            this.pB60.Location = new System.Drawing.Point(749, 360);
            this.pB60.Name = "pB60";
            this.pB60.Size = new System.Drawing.Size(74, 59);
            this.pB60.TabIndex = 73;
            this.pB60.TabStop = false;
            this.pB60.Click += new System.EventHandler(this.ChangeImage);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.value4);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.enemy4);
            this.panel1.Controls.Add(this.value3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.enemy3);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.enemy2);
            this.panel1.Controls.Add(this.enemy1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.value1);
            this.panel1.Controls.Add(this.value2);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(628, 93);
            this.panel1.TabIndex = 74;
            // 
            // value4
            // 
            this.value4.AutoSize = true;
            this.value4.Location = new System.Drawing.Point(423, 53);
            this.value4.Name = "value4";
            this.value4.Size = new System.Drawing.Size(13, 13);
            this.value4.TabIndex = 17;
            this.value4.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(372, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Flying";
            // 
            // enemy4
            // 
            this.enemy4.Location = new System.Drawing.Point(442, 45);
            this.enemy4.Name = "enemy4";
            this.enemy4.Size = new System.Drawing.Size(189, 45);
            this.enemy4.TabIndex = 15;
            this.enemy4.Scroll += new System.EventHandler(this.enemy4_Scroll);
            // 
            // value3
            // 
            this.value3.AutoSize = true;
            this.value3.Location = new System.Drawing.Point(423, 10);
            this.value3.Name = "value3";
            this.value3.Size = new System.Drawing.Size(13, 13);
            this.value3.TabIndex = 14;
            this.value3.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(368, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Shooting";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // enemy3
            // 
            this.enemy3.Location = new System.Drawing.Point(442, 0);
            this.enemy3.Name = "enemy3";
            this.enemy3.Size = new System.Drawing.Size(189, 45);
            this.enemy3.TabIndex = 12;
            this.enemy3.Scroll += new System.EventHandler(this.enemy3_Scroll);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(637, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(208, 93);
            this.panel2.TabIndex = 75;
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.pB24);
            this.panel3.Controls.Add(this.pB1);
            this.panel3.Controls.Add(this.pB2);
            this.panel3.Controls.Add(this.pB60);
            this.panel3.Controls.Add(this.pB3);
            this.panel3.Controls.Add(this.pB59);
            this.panel3.Controls.Add(this.pB4);
            this.panel3.Controls.Add(this.pB58);
            this.panel3.Controls.Add(this.pB5);
            this.panel3.Controls.Add(this.pB57);
            this.panel3.Controls.Add(this.pB6);
            this.panel3.Controls.Add(this.pB56);
            this.panel3.Controls.Add(this.pB7);
            this.panel3.Controls.Add(this.pB55);
            this.panel3.Controls.Add(this.pB8);
            this.panel3.Controls.Add(this.pB54);
            this.panel3.Controls.Add(this.pB9);
            this.panel3.Controls.Add(this.pB53);
            this.panel3.Controls.Add(this.pB10);
            this.panel3.Controls.Add(this.pB52);
            this.panel3.Controls.Add(this.pB11);
            this.panel3.Controls.Add(this.pB51);
            this.panel3.Controls.Add(this.pB12);
            this.panel3.Controls.Add(this.pB50);
            this.panel3.Controls.Add(this.pB13);
            this.panel3.Controls.Add(this.pB49);
            this.panel3.Controls.Add(this.pB14);
            this.panel3.Controls.Add(this.pB48);
            this.panel3.Controls.Add(this.pB15);
            this.panel3.Controls.Add(this.pB47);
            this.panel3.Controls.Add(this.pB16);
            this.panel3.Controls.Add(this.pB46);
            this.panel3.Controls.Add(this.pB17);
            this.panel3.Controls.Add(this.pB45);
            this.panel3.Controls.Add(this.pB18);
            this.panel3.Controls.Add(this.pB44);
            this.panel3.Controls.Add(this.pB19);
            this.panel3.Controls.Add(this.pB43);
            this.panel3.Controls.Add(this.pB20);
            this.panel3.Controls.Add(this.pB42);
            this.panel3.Controls.Add(this.pB21);
            this.panel3.Controls.Add(this.pB41);
            this.panel3.Controls.Add(this.pB22);
            this.panel3.Controls.Add(this.pB40);
            this.panel3.Controls.Add(this.pB23);
            this.panel3.Controls.Add(this.pB39);
            this.panel3.Controls.Add(this.pB25);
            this.panel3.Controls.Add(this.pB38);
            this.panel3.Controls.Add(this.pB26);
            this.panel3.Controls.Add(this.pB37);
            this.panel3.Controls.Add(this.pB27);
            this.panel3.Controls.Add(this.pB36);
            this.panel3.Controls.Add(this.pB28);
            this.panel3.Controls.Add(this.pB35);
            this.panel3.Controls.Add(this.pB29);
            this.panel3.Controls.Add(this.pB34);
            this.panel3.Controls.Add(this.pB30);
            this.panel3.Controls.Add(this.pB33);
            this.panel3.Controls.Add(this.pB31);
            this.panel3.Controls.Add(this.pB32);
            this.panel3.Location = new System.Drawing.Point(3, 102);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(849, 432);
            this.panel3.TabIndex = 76;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(366, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 26);
            this.label5.TabIndex = 74;
            this.label5.Text = "Map Editor";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(855, 544);
            this.tableLayoutPanel1.TabIndex = 77;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 607);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.loadButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.enemy1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB60)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enemy4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TrackBar enemy1;
        private System.Windows.Forms.TrackBar enemy2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label value1;
        private System.Windows.Forms.Label value2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkRanged;
        private System.Windows.Forms.CheckBox checkMelee;
        private System.Windows.Forms.PictureBox pB1;
        private System.Windows.Forms.PictureBox pB2;
        private System.Windows.Forms.PictureBox pB3;
        private System.Windows.Forms.PictureBox pB4;
        private System.Windows.Forms.PictureBox pB5;
        private System.Windows.Forms.PictureBox pB6;
        private System.Windows.Forms.PictureBox pB7;
        private System.Windows.Forms.PictureBox pB8;
        private System.Windows.Forms.PictureBox pB9;
        private System.Windows.Forms.PictureBox pB10;
        private System.Windows.Forms.PictureBox pB11;
        private System.Windows.Forms.PictureBox pB12;
        private System.Windows.Forms.PictureBox pB13;
        private System.Windows.Forms.PictureBox pB14;
        private System.Windows.Forms.PictureBox pB15;
        private System.Windows.Forms.PictureBox pB16;
        private System.Windows.Forms.PictureBox pB17;
        private System.Windows.Forms.PictureBox pB18;
        private System.Windows.Forms.PictureBox pB19;
        private System.Windows.Forms.PictureBox pB20;
        private System.Windows.Forms.PictureBox pB21;
        private System.Windows.Forms.PictureBox pB22;
        private System.Windows.Forms.PictureBox pB23;
        private System.Windows.Forms.PictureBox pB24;
        private System.Windows.Forms.PictureBox pB25;
        private System.Windows.Forms.PictureBox pB26;
        private System.Windows.Forms.PictureBox pB27;
        private System.Windows.Forms.PictureBox pB28;
        private System.Windows.Forms.PictureBox pB29;
        private System.Windows.Forms.PictureBox pB30;
        private System.Windows.Forms.PictureBox pB31;
        private System.Windows.Forms.PictureBox pB32;
        private System.Windows.Forms.PictureBox pB33;
        private System.Windows.Forms.PictureBox pB34;
        private System.Windows.Forms.PictureBox pB35;
        private System.Windows.Forms.PictureBox pB36;
        private System.Windows.Forms.PictureBox pB37;
        private System.Windows.Forms.PictureBox pB38;
        private System.Windows.Forms.PictureBox pB39;
        private System.Windows.Forms.PictureBox pB40;
        private System.Windows.Forms.PictureBox pB41;
        private System.Windows.Forms.PictureBox pB42;
        private System.Windows.Forms.PictureBox pB43;
        private System.Windows.Forms.PictureBox pB44;
        private System.Windows.Forms.PictureBox pB45;
        private System.Windows.Forms.PictureBox pB46;
        private System.Windows.Forms.PictureBox pB47;
        private System.Windows.Forms.PictureBox pB48;
        private System.Windows.Forms.PictureBox pB49;
        private System.Windows.Forms.PictureBox pB50;
        private System.Windows.Forms.PictureBox pB51;
        private System.Windows.Forms.PictureBox pB52;
        private System.Windows.Forms.PictureBox pB53;
        private System.Windows.Forms.PictureBox pB54;
        private System.Windows.Forms.PictureBox pB55;
        private System.Windows.Forms.PictureBox pB56;
        private System.Windows.Forms.PictureBox pB57;
        private System.Windows.Forms.PictureBox pB58;
        private System.Windows.Forms.PictureBox pB59;
        private System.Windows.Forms.PictureBox pB60;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar enemy3;
        private System.Windows.Forms.Label value4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TrackBar enemy4;
        private System.Windows.Forms.Label value3;
    }
}

