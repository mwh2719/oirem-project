﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ExtTool
{
    public partial class Form1 : Form
    {
        // Puts the trackbars in a list
        List<TrackBar> trackBars;

        // Puts checkboxes in a list
        List<CheckBox> checkBoxes;

        // Array of tiles
        PictureBox[,] tiles;

        public Form1()
        {
            InitializeComponent();
            trackBars = new List<TrackBar>();
            checkBoxes = new List<CheckBox>();
            trackBars.Add(enemy1);
            trackBars.Add(enemy2);
            trackBars.Add(enemy3);
            trackBars.Add(enemy4);
            checkBoxes.Add(checkMelee);
            checkBoxes.Add(checkRanged);
            PopulateArray();
        }

        // Loads file in
        private void loadButton_Click(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader(@"../../../../Oirem/Oirem/Content/game.txt"); // Loads file from content folder in Oirem project
            string[] line = sr.ReadToEnd().Split('\n'); // Reads all lines in the file

            // Looks through the lines and form to information in the file
            for (int i = 0; i < line.Length - 1; i++)
            {
                // Sets trackbars to what is in the file
                foreach (TrackBar t in trackBars)
                {
                    if (line[i].Contains(t.Name))
                    {
                        int index = line[i].IndexOf(' ');
                        Int32.TryParse(line[i].Remove(0, index), out int result);
                        t.Value = result;
                    }
                }

                // Sets checkboxes to what is in the file
                foreach (CheckBox c in checkBoxes)
                {
                    if (line[i].Contains(c.Text))
                    {
                        int index = line[i].IndexOf(' ');
                        bool.TryParse(line[i].Remove(0, index), out bool result);
                        c.Checked = result;
                    }
                }
            }

            // Displays the value of the trackbars
            value1.Text = trackBars[0].Value.ToString();
            value2.Text = trackBars[1].Value.ToString();
            value3.Text = trackBars[2].Value.ToString();
            value4.Text = trackBars[3].Value.ToString();

            // Loads in Map
            LoadMap(sr, line);

            sr.Close(); // Closes stream reader
        }

        // Saves config to file
        private void saveButton_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter(@"../../../../Oirem/Oirem/Content/game.txt"); // Writes file to Content folder in Oirem project

            // Writes the property and value of the trackbars
            sw.WriteLine("Enemy Options:");
            foreach (TrackBar t in trackBars)
            {
                sw.WriteLine("{0}: {1}", t.Name, t.Value);
            }

            // Writes the the staus of the checkboxes
            sw.WriteLine("Player Options:");
            foreach (CheckBox c in checkBoxes)
            {
                sw.WriteLine("{0}: {1}", c.Text, c.Checked);
            }
            sw.WriteLine();

            // Saves Map
            SaveMap(sw);

            sw.Close(); // Closes stream writer
        }

        // Displays the values of the trackbars as they change
        private void enemy1_Scroll(object sender, EventArgs e)
        {
            value1.Text = enemy1.Value.ToString();
        }
        private void enemy2_Scroll(object sender, EventArgs e)
        {
            value2.Text = enemy2.Value.ToString();
        }
        private void enemy3_Scroll(object sender, EventArgs e)
        {
            value3.Text = enemy3.Value.ToString();
        }
        private void enemy4_Scroll(object sender, EventArgs e)
        {
            value4.Text = enemy4.Value.ToString();
        }

        // Changes tile being displayed when clicked on
        private void ChangeImage(object sender, EventArgs e)
        {
            if (sender is PictureBox)
            {
                PictureBox currentImg = sender as PictureBox;
                string path = @"../../../../Oirem/Oirem/Content/png/Tiles/1.png"; // Default file path

                // Displays first tile if it's currently blank
                if (currentImg.ImageLocation == null)
                {
                    currentImg.ImageLocation = path;
                }
                // If tile being shown is not blamk
                else
                {
                    // Gets the name of the image being displayed
                    string otherPath = currentImg.ImageLocation;
                    otherPath = otherPath.Substring(otherPath.IndexOf("Tiles/"));
                    otherPath = otherPath.Remove(otherPath.IndexOf(".")).Remove(0, 6);
                    Int32.TryParse(otherPath, out int imgNum);

                    // Makes the tile blank after the last tile in folder
                    if (imgNum == 16)
                    {
                        path = null;
                    }
                    // Goes to next tile 
                    else
                    {
                        path = @"../../../../Oirem/Oirem/Content/png/Tiles/" + (imgNum + 1) + ".png";
                    }
                }
                currentImg.SizeMode = PictureBoxSizeMode.StretchImage; // Fits the image into the pictureBox
                currentImg.ImageLocation = path; // Displays image
            }
        }

        // Makes array of pictureBoxes to display tiles
        private void PopulateArray()
        {
            tiles = new PictureBox[,]
            {
                { pB1, pB2, pB3, pB4, pB5, pB6, pB7, pB8, pB9, pB10 },
                { pB11, pB12, pB13, pB14, pB15, pB16, pB17, pB18, pB19, pB20 },
                { pB21, pB22, pB23, pB24, pB25, pB26, pB27, pB28, pB29, pB30 },
                { pB31, pB32, pB33, pB34, pB35, pB36, pB37, pB38, pB39, pB40 },
                { pB41, pB42, pB43, pB44, pB45, pB46, pB47, pB48, pB49, pB50 },
                { pB51, pB52, pB53, pB54, pB55, pB56, pB57, pB58, pB59, pB60 }
            };
        }

        // Saves map to file
        private void SaveMap(StreamWriter sw)
        {
            int[,] tileNum = new int[tiles.GetLongLength(0), tiles.GetLength(1)]; // Numerical version of Map
            sw.WriteLine("Map:");

            // Checks tile being displayed and prints numerical representation
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    // Empty tile
                    if (tiles[i, j].ImageLocation == null)
                    {
                        tileNum[i, j] = 0;
                    }
                    // Non-empty tile
                    else
                    {
                        string tileName = tiles[i, j].ImageLocation; // Name of tile displayed
                        tileName = tileName.Substring(tileName.IndexOf("Tiles/"));
                        tileName = tileName.Remove(tileName.IndexOf(".")).Remove(0, 6);
                        Int32.TryParse(tileName, out tileNum[i, j]);
                    }
                    sw.Write("["+tileNum[i, j]+"]");
                }
                sw.WriteLine(); 
            }
        }

        // Loads map from file
        private void LoadMap(StreamReader sr, string[] line)
        {
            string[] mapLines = new string[6]; // Saves the lines for Map
            int[,] tileNum = new int[tiles.GetLongLength(0), tiles.GetLength(1)]; // Numerical version of Map
            int index = 0; // Index for mapLines
            for (int i = 0; i < line.Length; i++)
            {
                if (!line[i].StartsWith("["))
                {
                    continue;
                }
                else
                {
                    mapLines[index] = line[i].Replace("[", ""); // Removes the first part of array of numbers
                    string[] nums = mapLines[index].Split(']'); // Splits the numbers up

                    // Adds them to numerical version of map
                    for (int j = 0; j < tileNum.GetLength(1); j++)
                    {
                        Int32.TryParse(nums[j], out tileNum[index, j]);
                    }
                    index++;
                }
            }

            // Displays tiles on screen
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    string path = @"../../../../Oirem/Oirem/Content/png/Tiles/" + tileNum[i, j] + ".png"; // Looks for image of tile

                    // Empty tiles
                    if (tileNum[i, j] == 0)
                    {
                        path = null;
                    }
                    tiles[i, j].SizeMode = PictureBoxSizeMode.StretchImage;
                    tiles[i, j].ImageLocation = path;
                }
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

    }
}
