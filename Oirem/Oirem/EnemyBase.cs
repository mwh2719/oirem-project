﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class EnemyBase
    {
        //Base attributes
        protected int enemyHealth;
        protected int movementSpeed;
        protected double timeToJump, timeToShoot;
        protected Rectangle enemyRectangle;
        protected Texture2D spriteSheet;
        protected Vector2 velocity, landing, fall;
        protected bool isJumping;
        protected bool bee;
        protected bool jumper;
        protected bool onGround; // Checks whether or not the enemy is in the air

        //Make an object for projectile class
        protected Projectile enemyProjectile;
        protected int projectileSpeed;
        protected int intialY;
        protected List<Projectile> enemyProj = new List<Projectile>();
        protected bool jump;
        protected bool shoot;

        //attribute to hold whether the enemey has moved already
        protected bool hasMoved;

        // For animations
        protected Rectangle source;
        protected Rectangle destination;
        protected int currentFrame;
        protected int totalFrames;
        protected int originalWidth;
        protected int originalHeight;
        protected int y;
        protected int offset;
        protected int framesElapsed;
        protected double timePerFrame;

        //Properties for base attributes
        public int Type { get { return Type; } set { Type = value; } }
        public int EnemyHealth { get { return enemyHealth; } }
        public int MovementSpeed { get { return movementSpeed; } }
        public int PosX { get { return enemyRectangle.X; } set { enemyRectangle.X = value; } }
        public int PosY { get { return enemyRectangle.Y; } set { enemyRectangle.Y = value; } }
        public Rectangle EnemyRectangle { get { return enemyRectangle; } set { enemyRectangle = value; } }
        public double TimeToJump { get { return timeToJump; } }
        public double TimeToShoot { get { return timeToShoot; } }
        public bool HasMoved { get { return hasMoved; } }
        public bool Jump { get { return jump; } }
        public bool Shoot { get { return shoot; } }
        public List<Projectile> EnemyProj {  get { return enemyProj; } }
        public bool Bee { get { return bee; } }

        //defualt constructor
        public EnemyBase(int xLoc, int yLoc)
        {
            currentFrame = 0;
            movementSpeed = 2;
            intialY = yLoc;
        }

        // Updates positon
        public virtual void Update(GraphicsDevice g1, GameTime gameTime, List<Tile> map)
        {
            if (currentFrame > totalFrames)
                currentFrame = 0;

            source = new Rectangle((originalWidth + offset) * currentFrame, y, originalWidth, originalHeight);

            Rectangle prev = enemyRectangle;

            enemyRectangle.X += movementSpeed;

            if (PosY > g1.Viewport.Height)
            {
                PosY = 0 - EnemyRectangle.Height;
            }

            hasMoved = true;

            if (PosX > g1.Viewport.Width)
            {
                PosX = 0 - EnemyRectangle.Width;
                PosY = intialY;
            }

            enemyRectangle.Location += velocity.ToPoint();
            if (bee == false)
            {
                onGround = false;
            }
            foreach (Tile t in map)
            {
                // If enemy touches tile
                if (t.CheckCollision(enemyRectangle, prev))
                {
                    // If tile is a ground tile
                    if (t.Corner || t.Ground || t.Platform && t.Active)
                    {
                        onGround = true;
                        enemyRectangle.Y = t.CurrentTile.Y - enemyRectangle.Height;
                    }
                    // If tile is a wall tile
                    if (t.Wall)
                    {
                        if (t.RightWall)
                        {
                            enemyRectangle.X = t.CurrentTile.X + t.CurrentTile.Width;
                        }
                        else
                        {
                            enemyRectangle.X = t.CurrentTile.X - enemyRectangle.Width;
                        }
                    }
                }
            }
                if (onGround)
                {
                    isJumping = false;
                }
                else
                {
                    isJumping = true;
                }

                if(enemyRectangle.X < 0 - enemyRectangle.Width)
            {
                isJumping = false;
            }

            if (!isJumping)
            {
                velocity.Y = 0;
            }
            else
            {
                int i = 1;
                velocity.Y += .4f * i;
            }
            
        }

        // Draw
        public virtual void Draw(SpriteBatch spriteBatch, Texture2D sprite)
        {
            spriteSheet = sprite;
            spriteBatch.Draw(spriteSheet, enemyRectangle, source, Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
            Console.WriteLine(currentFrame);
        }

        public void TakeDamage(Player player, EnemyBase enemy, Projectile projectile)
        {
            if (player.PlayerRectangle.Intersects(enemy.EnemyRectangle))
            {
                enemyHealth--;

            }

            if (projectile != null)
            {
                if (projectile.ProjectileRectangle.Intersects(enemy.enemyRectangle))
                {
                    enemyHealth--;
                }
            }
        }
    }
}
