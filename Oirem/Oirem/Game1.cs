﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MonoGame.Extended;
using MonoGame.Extended.ViewportAdapters;

namespace Oirem
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //attributes to hold sprites for map, player and enemy
        Texture2D map;
        Texture2D player;
        Texture2D enemy;
        Texture2D jumpEnemy;
        Texture2D shootEnemy;
        Texture2D jumpShootEnemy;
        Texture2D playerProjectile;

        // Background
        Texture2D background;

        //making player map and enenmy
        Player p1;
        Projectile pProjectile;

        Texture2D health;

        // Camera
        Camera2D camera;

        // Enemy spawner
        Spawner spawner;
        List<EnemyBase> enemies;


        //keyboard states
        KeyboardState kbState;
        KeyboardState oldKbState;

        // Mouse States
        MouseState mouse;
        MouseState oldMouse;

        //stuff needed for menu
        SpriteFont font;
        Texture2D button;
        Texture2D overlay;
        Texture2D instrImg;
        Point screenCenter;
        Rectangle windowSize;
        MainMenu menu;
        Instructions instructions;
        GameScreen game;
        PauseMenu pause;
        OptionsMenu options;
        GameOver gameOver;
        Victory victory;

        // Tiles 
        List<Texture2D> tileSet;

        //List of enemey projectiles
        List<Projectile> walkEnemyShoot = new List<Projectile>();
        List<Projectile> jumpEnemyShoot = new List<Projectile>();

        // Music
        Song song;

        Texture2D enemyBullet;

        // Game states
        enum Choices
        {
            MainMenu,
            Instructions,
            Options,
            PlayGame,
            PauseMenu,
            GameOver,
            Victory,
            Quit
        }

        Choices choice;
        Choices prev;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            // Initializing the camera
            ViewportAdapter viewportAdapter = new BoxingViewportAdapter(Window, GraphicsDevice, GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height);
            camera = new Camera2D(viewportAdapter);

            enemies = new List<EnemyBase>();

            //Enemy spawner
            /*spawner = new Spawner();
            enemies = spawner.SpawnEnemies(game.TileMap);*/


            IsMouseVisible = true;


            tileSet = new List<Texture2D>();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //loading map pieces
            map = Content.Load<Texture2D>("grass");

            // Loading background
            background = Content.Load<Texture2D>("png/background");

            //loading player
            player = Content.Load<Texture2D>("sorlo super sheet");
            p1 = new Player(player);

            //loading enemies
            enemy = Content.Load<Texture2D>("Enemies/normal_spriteSheet");
            jumpEnemy = Content.Load<Texture2D>("Enemies/jump_spriteSheet");
            shootEnemy = Content.Load<Texture2D>("Enemies/shoot_spriteSheet");
            jumpShootEnemy = Content.Load<Texture2D>("Enemies/flying_spriteSheet");

            //loading heart
            health = Content.Load<Texture2D>("heartpixelart32x32");

            //loading things needed for the menus
            font = Content.Load<SpriteFont>("mainFont");
            button = Content.Load<Texture2D>("Parallelogon_rectangle");
            overlay = Content.Load<Texture2D>("Colorful_rectangle");
            //song = Content.Load<Song>("Music/Defense Line");
            instrImg = Content.Load<Texture2D>("png/Instructions");

            enemyBullet = Content.Load<Texture2D>("bullet3");

            // Tiles for map
            for (int i = 0; i < 16; i++)
            {
                tileSet.Add(Content.Load<Texture2D>(@"png/Tiles/" + (i + 1)));
            }

            //song = Content.Load<Song>(@"Music/Defense Line");
            screenCenter = new Point(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
            windowSize = GraphicsDevice.Viewport.Bounds;
            menu = new MainMenu(button, overlay, screenCenter, windowSize, camera);
            instructions = new Instructions(button, overlay, screenCenter, windowSize, camera, instrImg);
            game = new GameScreen(button, overlay, screenCenter, windowSize, camera, tileSet);
            pause = new PauseMenu(button, overlay, screenCenter, windowSize, camera);
            options = new OptionsMenu(button, overlay, screenCenter, windowSize, camera, song);
            gameOver = new GameOver(button, overlay, screenCenter, windowSize, camera);
            victory = new Victory(button, overlay, screenCenter, windowSize, camera);
            choice = Choices.MainMenu;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            mouse = Mouse.GetState();
            kbState = Keyboard.GetState();

            //stuff for menu
            switch (choice)
            {
                case Choices.Quit:
                    Exit();
                    break;
                case Choices.MainMenu:
                    camera.LookAt(screenCenter.ToVector2());
                    if (menu.Play.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Instructions;
                    }
                    if (menu.Options.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        prev = Choices.MainMenu;
                        choice = Choices.Options;
                    }
                    if (menu.Quit.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Quit;
                    }
                    break;
                case Choices.Instructions:
                    if (instructions.Play.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        Reset();
                        choice = Choices.PlayGame;
                    }
                    break;
                case Choices.Options:
                    camera.LookAt(screenCenter.ToVector2());
                    options.Update(mouse, oldMouse);
                    if (options.Main.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    if(options.Back.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = prev;
                    }
                    break;
                case Choices.PlayGame:

                    //updating eneimies and movement when the game starts
                    p1.Update(kbState, oldKbState, gameTime, GraphicsDevice, game.MapTiles);

                    //having a gameover if the player falls off the screen
                    if(p1.PosY > GraphicsDevice.Viewport.Height)
                    {
                        choice = Choices.GameOver;
                    }


                    if (enemies.Count != 0)
                    {
                        foreach (EnemyBase e in enemies)
                        {
                            e.Update(GraphicsDevice, gameTime, game.MapTiles);
                        }
                    }


                    if (kbState.IsKeyDown(Keys.P) && oldKbState.IsKeyUp(Keys.P))
                    {
                        choice = Choices.PauseMenu;
                    }
                    // For Testing
                    if (p1.PlayerHealth <= 0)
                    {
                        choice = Choices.GameOver;
                    }

                    //loop to check if player and enemies run into each other

                    // Creates the hitboxes on the player
                    p1.PlayerLeft = new Rectangle(p1.PlayerRectangle.Left, p1.PlayerRectangle.Top + 20, 2, 2);
                    p1.PlayerRight = new Rectangle(p1.PlayerRectangle.Right, p1.PlayerRectangle.Top + 20, 2, 2);

                    for (int i = 0; i < enemies.Count; i++)
                    {

                        // Attacking right, hitting enemy right
                        if (p1.IsAttacking == true && p1.FaceRight == true && p1.PlayerRight.Intersects(enemies[i].EnemyRectangle))
                        {
                            enemies.RemoveAt(i);
                        }
                        // Attacking left, hitting enemy left
                        else if (p1.IsAttacking == true && p1.FaceLeft == true && p1.PlayerLeft.Intersects(enemies[i].EnemyRectangle))
                        {
                            enemies.RemoveAt(i);
                        }
                        // Attacking left, getting hit right
                        else if (p1.IsAttacking == true && p1.FaceLeft == true && p1.PlayerRight.Intersects(enemies[i].EnemyRectangle))
                        {
                            p1.PlayerHealth--;
                            enemies.RemoveAt(i);
                        }
                        // Attacking right, getting hit left
                        else if (p1.IsAttacking == true && p1.FaceRight == true && p1.PlayerLeft.Intersects(enemies[i].EnemyRectangle))
                        {
                            p1.PlayerHealth--;
                            enemies.RemoveAt(i);
                        }
                        // Not attacking, getting hit
                        else if (p1.IsAttacking == false && p1.PlayerRectangle.Intersects(enemies[i].EnemyRectangle))
                        {
                            p1.PlayerHealth--;
                            enemies.RemoveAt(i);
                        }

                    }

                    for (int i = 0; i < enemies.Count; i++)
                    {
                        // Changes the bees location based on the players
                            if (enemies[i].Bee == true)
                        {
                            if (enemies[i].EnemyRectangle.X <= p1.PlayerRectangle.X && p1.PlayerRectangle.Y > enemies[i].EnemyRectangle.Y)
                            {
                                enemies[i].PosY++;
                            }
                            else if (enemies[i].EnemyRectangle.X <= p1.PlayerRectangle.X && p1.PlayerRectangle.Y < enemies[i].EnemyRectangle.Y)
                            {
                                enemies[i].PosY--;
                            }
                            else if (enemies[i].EnemyRectangle.Right == GraphicsDevice.Viewport.Width)
                            {
                                enemies[i].PosX--;
                            }
                        }
                    }

                    // Projectile shooting & hitting player
                    foreach (EnemyBase e in enemies)
                    {
                        for (int i = 0; i < e.EnemyProj.Count; i++)
                        {
                            if (e.EnemyProj[i].ProjectileRectangle.Intersects(p1.PlayerRectangle))
                            {
                                p1.PlayerHealth--;
                                e.EnemyProj.RemoveAt(i);
                            }
                        }
                    }

              //   if (enemyController >= (enemies.Count - 1))
              //   {
              //       enemyController = 0;
              //   }
              //   else
              //   {
              //       enemyController++;
              //   }

                    for(int i = 0; i < enemies.Count; i++)
                    {
                        if (p1.P1.ProjectileRectangle.Intersects(enemies[i].EnemyRectangle))
                        {
                            enemies.RemoveAt(i);
                            p1.P1.ProjectileActive = false;
                        }
                    }                  

                    if (p1.PosY > GraphicsDevice.Viewport.Height)
                    {
                        choice = Choices.GameOver;
                    }

                    if(enemies.Count == 0)
                    {
                        if (p1.PlayerHealth <= 0)
                        {
                            choice = Choices.GameOver;
                        }
                        else
                        {
                            choice = Choices.Victory;
                        }
                    }
                    
                    break;
                case Choices.Victory:
                    //victory.Update();
                    camera.LookAt(screenCenter.ToVector2());
                    if (victory.Retry.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        Reset();
                        choice = Choices.PlayGame;
                    }
                    if (victory.MainMenu.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    break;
                case Choices.PauseMenu:
                    //pause.Update();
                    camera.LookAt(screenCenter.ToVector2());
                    if (pause.Resume.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.PlayGame;
                    }
                    if (pause.Options.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        prev = Choices.PauseMenu;
                        choice = Choices.Options;
                    }
                    if (pause.Main.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    if (pause.Quit.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Quit;
                    }
                    break;
                case Choices.GameOver:
                    //gameOver.Update();
                    camera.LookAt(screenCenter.ToVector2());
                    if (gameOver.Retry.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        Reset();
                        choice = Choices.PlayGame;
                    }
                    if (gameOver.MainMenu.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    break;
            }

            base.Update(gameTime);

            oldMouse = mouse;
            oldKbState = kbState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            //drawing stuff for menu
            spriteBatch.Begin();

            switch (choice)
            {
                case Choices.MainMenu:
                    menu.Draw(spriteBatch, font);
                    break;
                case Choices.Instructions:
                    instructions.Draw(spriteBatch, font);
                    break;
                case Choices.Options:
                    options.Draw(spriteBatch, font);
                    break;
                case Choices.PlayGame:

                    // Drawing background
                    spriteBatch.Draw(background, new Vector2(0.0f, -50.0f), Color.White);

                    if(p1.PlayerHealth == 3)
                    {
                        spriteBatch.Draw(health, new Vector2(85, 0), Color.White);
                        spriteBatch.Draw(health, new Vector2(120, 0), Color.White);
                        spriteBatch.Draw(health, new Vector2(155, 0), Color.White);
                    }
                    else if(p1.PlayerHealth == 2)
                    {
                        spriteBatch.Draw(health, new Vector2(85, 0), Color.White);
                        spriteBatch.Draw(health, new Vector2(120, 0), Color.White);
                    }
                    else if(p1.PlayerHealth == 1)
                    {
                        spriteBatch.Draw(health, new Vector2(85, 0), Color.White);
                    }
                    
                    /*
                    if (walkEnemyShoot.Count > 0)
                    {
                        foreach (Projectile p in walkEnemyShoot)
                        {
                            if (p != null)
                            {
                                spriteBatch.Draw(enemyBullet, p.ProjectileRectangle, Color.White);
                            }
                        }
                    }
                    if (jumpEnemyShoot.Count > 0)
                    {
                        foreach (Projectile p in jumpEnemyShoot)
                        {
                            if (p != null)
                            {
                                spriteBatch.Draw(enemyBullet, p.ProjectileRectangle, Color.White);
                            }
                        }
                    }*/

                    game.Draw(spriteBatch, font);

                    //drawing player
                    p1.Draw(spriteBatch);

                    //draws multiple enemies
                    if (enemies.Count != 0)
                    {
                        foreach (EnemyBase e in enemies)
                        {
                            if (e is OrangeGoblin)
                                e.Draw(spriteBatch, jumpEnemy);
                            if (e is LittleMonster)
                                e.Draw(spriteBatch, shootEnemy);
                            if (e is RedBee)
                                e.Draw(spriteBatch, jumpShootEnemy);
                            if (e is GreenMonster)
                                e.Draw(spriteBatch, enemy);
                        }
                    }
                    if (enemies.Count != 0)
                    {
                        foreach (EnemyBase e in enemies)
                        {
                            foreach (Projectile p in e.EnemyProj)
                            {
                                if (p.ProjectileActive == true)
                                {
                                    spriteBatch.Draw(enemyBullet, p.ProjectileRectangle, Color.White);
                                }
                            }
                        }
                    }
                    break;
                case Choices.Victory:
                    victory.Draw(spriteBatch, font);
                    break;
                case Choices.PauseMenu:
                    pause.Draw(spriteBatch, font);
                    break;
                case Choices.GameOver:
                    gameOver.Draw(spriteBatch, font);
                    break;
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        //method to reset the game after playing
        public void Reset()
        {
            //Enemy spawner
            spawner = new Spawner();
            enemies = spawner.SpawnEnemies(game.MapTiles);
            p1.P1.ProjectileActive = false;

            p1.PlayerHealth = 3;
            p1.DistanceTraveled = 0;

            p1.PosX = 250;
            p1.PosY = 300;
        }

    }
}
