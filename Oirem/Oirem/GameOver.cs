﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Displays once player has lost all health
    /// </summary>
    class GameOver : MenuManager
    {
        // Attributes
        Rectangle retry;
        Rectangle mainMenu;

        // Properties
        public Rectangle Retry { get { return retry; } set { retry = value; } }
        public Rectangle MainMenu { get { return mainMenu; } set { mainMenu = value; } }

        // Constuctor
        public GameOver(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window, Camera2D camera) : base(button, overlay, screenCtr, window, camera)
        {

        }

        // Draws Game Over screen
        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.DrawString(font, "Game Over", CenterText(font, "Game Over", window) - new Vector2(0, 150), Color.Black);
            retry = DrawButton(spriteBatch, retry, new Point(0, 50), font, "Retry");
            mainMenu = DrawButton(spriteBatch, mainMenu, new Point(0, 100), font, "Main Menu");
        }
    }
}
