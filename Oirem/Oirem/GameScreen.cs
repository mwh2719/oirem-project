﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Where the game actually takes place
    /// </summary>
    class GameScreen : MenuManager
    {
        // Attributes
        private Spawner map; // Imports map made in Spawner
        private List<Texture2D> tileSet; // List of all available tiles
        private List<Tile> mapTiles; // All tiles on map
        private const int SIZE = 80; // Size of all tiles
        private Rectangle[,] tileMap;
        private List<Rectangle> intersectingPlatforms = new List<Rectangle>();
        
        public List<Tile> MapTiles { get { return mapTiles; } }
        public Rectangle[,] TileMap { get { return tileMap; } }

        public GameScreen(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window, Camera2D camera, List<Texture2D> tS) : base(button, overlay, screenCtr, window, camera)
        {
            map = new Spawner();
            mapTiles = new List<Tile>();
            tileSet = tS;
            tileMap = new Rectangle[map.MapSpawner.GetLength(0), map.MapSpawner.GetLength(1)];

            // Adds all tiles in the map to the list 
            for (int i = 0; i < map.MapSpawner.GetLength(0); i++)
            {
                for (int j = 0; j < map.MapSpawner.GetLength(1); j++)
                {
                    if (map.MapSpawner[i, j] != 0)
                    {
                        Texture2D tile = tileSet[map.MapSpawner[i, j] - 1];
                        tileMap[i, j] = new Rectangle(SIZE * j, SIZE * i, SIZE, SIZE);
                        mapTiles.Add(new Tile(tile, new Rectangle(SIZE * j, SIZE * i, SIZE, SIZE), map.MapSpawner[i, j]));
                    }
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.DrawString(font, "Health:", new Vector2(0), Color.Black);

            // Draws map
            foreach (Tile t in mapTiles)
            {
                spriteBatch.Draw(t.Texture, t.CurrentTile, Color.White);
            }
        }
    }
}
