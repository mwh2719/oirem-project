﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class Gravity
    {
        //method to apply gravity to both the player and the enemy by passing in their rectangles
        public void FallingGravity(Player p1, Rectangle ground, List<Platform> platforms)
        {
            bool intersectingStuff = false;

            for(int i = 0; i < platforms.Count; i++)
            {
                if (p1.PlayerRectangle.Intersects(platforms[i].PlatformRectangle) && (p1.PlayerRectangle.Y + p1.PlayerRectangle.Height - 2) <= platforms[i].PosY)
                {
                    intersectingStuff = true;
                }
            }

            if ((p1.PosY + p1.PlayerRectangle.Height) <= ground.Y && intersectingStuff == false)
            {
                p1.PosY += 2;
            }
        }

        public void FallingGravity(EnemyBase e1, Rectangle ground, List<Platform> platforms)
        {
            bool intersectingStuff = false;

            for (int i = 0; i < platforms.Count; i++)
            {
                if (e1.EnemyRectangle.Intersects(platforms[i].PlatformRectangle) && (e1.EnemyRectangle.Y + e1.EnemyRectangle.Height - 2) <= platforms[i].PosY)
                {
                    intersectingStuff = true;
                }
            }

            if ((e1.PosY + e1.EnemyRectangle.Height) <= ground.Y && intersectingStuff == false)
            {
                e1.PosY += 2;
            }
        }



    }
}
