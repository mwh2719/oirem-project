﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Oirem
{
    /// <summary>
    /// Normal enemy
    /// </summary>
    class GreenMonster : EnemyBase
    {
        public GreenMonster(int xLoc, int yLoc) : base(xLoc, yLoc)
        {
            intialY = yLoc;
            originalHeight = 535;
            originalWidth = 484;
            y = 23;
            totalFrames = 6;
            timePerFrame = 150;
            offset = 54;
            movementSpeed = 3;
            enemyRectangle = new Rectangle(xLoc, yLoc, 50, 50);
        }

        public override void Update(GraphicsDevice g1, GameTime gameTime, List<Tile> map)
        {
            framesElapsed = (int)(gameTime.TotalGameTime.TotalMilliseconds / timePerFrame);
            currentFrame = framesElapsed % totalFrames;

            base.Update(g1, gameTime, map);
        }
    }
}
