﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Instructions screen
    /// </summary>
    class Instructions : MenuManager
    {
        private Rectangle play;
        private Texture2D img; // Image of instructions

        public Rectangle Play { get { return play; } }

        public Instructions(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window, Camera2D camera, Texture2D img) : base(button, overlay, screenCtr, window, camera)
        {
            this.img = img;
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(img, new Rectangle(0, 0, img.Width, img.Height), Color.White);
            play = DrawButton(spriteBatch, play, new Point(0, 175), font, "Play");
        }
    }
}
