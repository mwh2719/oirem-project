﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Oirem
{
    /// <summary>
    /// Enemy that just shoots
    /// </summary>
    class LittleMonster : EnemyBase
    {
        private double timer = 2;

        // Constructor
        public LittleMonster(int xLoc, int yLoc) : base(xLoc, yLoc)
        {
            intialY = yLoc;
            originalWidth = 484;
            originalHeight = 523;
            y = 30;
            offset = 91;
            totalFrames = 8;
            timePerFrame = 150;
            timeToShoot = 2;
            enemyRectangle = new Rectangle(xLoc, yLoc, 50, 50);
        }

        // Update for monster
        public override void Update(GraphicsDevice g1, GameTime gameTime, List<Tile> map)
        {
            timer += gameTime.ElapsedGameTime.TotalSeconds;
            framesElapsed = (int)(gameTime.TotalGameTime.TotalMilliseconds / timePerFrame);
            currentFrame = framesElapsed % totalFrames;

            

            if (timer >= timeToShoot)
            {
                timer = 0.0;
                enemyProj.Add(new Projectile(enemyRectangle));
            }

            foreach (Projectile p in enemyProj)
            {
                if (p.ProjectileActive == true)
                {
                    p.ProjectileMovement(g1);
                }
            }
            base.Update(g1, gameTime, map);
        }
    }
}
