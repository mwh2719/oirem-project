﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Provides access to other screens
    /// </summary>
    class MainMenu : MenuManager
    {
        // Attributes
        private Rectangle play; 
        private Rectangle options; 
        private Rectangle quit;

        // Properties
        public Rectangle Play { get { return play; } set { play = value; } }
        public Rectangle Options { get { return options; } }
        public Rectangle Quit { get { return quit; } }

        // Constructor
        public MainMenu(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window, Camera2D camera) : base(button, overlay, screenCtr, window, camera)
        {

        }

        // Draws Main Menu
        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.DrawString(font, "Defeat all the enemies without getting hit ", new Vector2(150, 150), Color.Black);
            spriteBatch.DrawString(font, "three times in order to win", new Vector2(245, 180), Color.Black);
            spriteBatch.DrawString(font, "Oirem", CenterText(font, "Oirem", window) - new Vector2(0, 150), Color.Black);
            play = DrawButton(spriteBatch, play, new Point(0, 50), font, "Play");
            options =DrawButton(spriteBatch, options, new Point(0, 100), font, "Options");
            quit = DrawButton(spriteBatch, quit, new Point(0, 150), font, "Quit");
        }
    }
}
