﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Handles all of the attributes for the menus and their buttons
    /// </summary>
    public class MenuManager
    {
        // Attributes
        protected Texture2D button;
        protected Texture2D overlay;
        protected Point size;
        protected Point location;
        protected Rectangle rectangle;
        protected Rectangle window;
        protected Point screenCtr;
        protected Camera2D camera;

        // Properties
        public Texture2D Button { get { return button; } }
        public Point Size { get { return size; } }
        public Point Location { get { return location; } }
        public Rectangle Rectangle { get { return rectangle; } }
        public int X { get { return rectangle.X; } set { rectangle.X = value; } }
        public int Y { get { return rectangle.Y; } set { rectangle.Y = value; } }

        // Default constructor
        public MenuManager(Texture2D button, Texture2D overlay, Point screenCenter, Rectangle window, Camera2D camera)
        {
            this.overlay = overlay; // Where buttons will be placed (check PauseMenu for application)
            screenCtr = screenCenter;

            // Default button properties
            this.button = button;
            size = new Point(150, 50);
            location = new Point(350, 50);
            rectangle = new Rectangle(location, size);

            this.window = window;

            this.camera = camera;
        }
        
        // Overridable draw method
        public virtual void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            
        }

        // Centers rectangle on screen
        public void CenterOnScreen(SpriteBatch spriteBatch, Rectangle rectangle, Point discplacement)
        {
            Point dist = this.rectangle.Location - this.rectangle.Center;
            Vector2 camCtr = new Vector2(camera.BoundingRectangle.Center.X, camera.BoundingRectangle.Center.Y);
            Point center = camCtr.ToPoint() + dist;
            rectangle = new Rectangle(center + discplacement, size);
            spriteBatch.Draw(button, rectangle, Color.White);
        }

        // Centers text in object
        public Vector2 CenterText(SpriteFont font, String text, Rectangle rectangle)
        {
            Vector2 txtCntr = font.MeasureString(text) / 2;
            Vector2 center = new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2) - txtCntr;
            return center;
        }

        // Draws button centered on screen
        public Rectangle DrawButton(SpriteBatch spriteBatch, Rectangle rectangle, Point displacement, SpriteFont font, String text)
        {
            Point dist = this.rectangle.Location - this.rectangle.Center;
            //Vector2 camCtr = new Vector2(camera.BoundingRectangle.Center.X, camera.BoundingRectangle.Center.Y);
            //Point center = camCtr.ToPoint() + dist;
            Point center = screenCtr + dist;
            rectangle = new Rectangle(center + displacement, size);
            Vector2 txtCntr = new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2) - font.MeasureString(text) / 2;
            spriteBatch.Draw(button, rectangle, Color.White);
            spriteBatch.DrawString(font, text, txtCntr, Color.Black);
            return rectangle;
        }

        public Rectangle DrawButton(SpriteBatch spriteBatch, Point size, Point location, SpriteFont font, String text)
        {
            Rectangle rectangle = new Rectangle(location, size);
            Vector2 txtCntr = new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2) - font.MeasureString(text) / 2;
            spriteBatch.Draw(button, rectangle, Color.White);
            spriteBatch.DrawString(font, text, txtCntr, Color.Black);
            return rectangle;
        }
    }
}
