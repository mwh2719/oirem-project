﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class Obstacle
    {

        //attribute of obstacle size and skin
        Rectangle obstacleRectangle;
        Texture2D obstacleSkin;

        //constructor 
        public Obstacle(int x, int y, int width, int height, Texture2D sprite)
        {
            obstacleRectangle = new Rectangle(x, y, width, height);
            obstacleSkin = sprite;
        }

        //property for obstacle rectangle
        public Rectangle ObstacleRectangle
        {
            get { return obstacleRectangle; }
        }

        //property for texture
        public Texture2D ObstacleSkin
        {
            get { return obstacleSkin; }
        }

        //checking for collison between an obstacle and player
        public void CheckCollsion(Rectangle r1)
        {
            if (obstacleRectangle.Intersects(r1))
            {
                //do stuff here, probably damage player
            }
        }

    }
}
