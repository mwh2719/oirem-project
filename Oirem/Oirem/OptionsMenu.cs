﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Allows user to chnage window properties and sound properties
    /// (Under construction)
    /// </summary>
    class OptionsMenu : MenuManager
    {
        // Attributes
        private Rectangle main;
        private Rectangle music;
        private Rectangle video;
        private Rectangle back;
        private Song song;
        private bool soundOn;

        // Properties
        public Rectangle Main { get { return main; } set { main = value; } }
        public Rectangle Music { get { return music; } set { music = value; } }
        public Rectangle Video { get { return video; } set { video = value; } }
        public Rectangle Back { get { return back; } set { back = value; } }

        // Contructor
        public OptionsMenu(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window, Camera2D camera, Song song) : base(button, overlay, screenCtr, window, camera)
        {
            this.song = song;
            soundOn = false;
        }

        // Update method used for changing settings
        public void Update(MouseState mouse, MouseState oldMouse)
        {
            // Turns on music when button is pressed
            if (music.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
            {
                if (!soundOn)
                {
                    MediaPlayer.Play(song);
                    MediaPlayer.IsRepeating = true;
                    soundOn = true;
                }
                // Turns off music when button is pressed
                else
                {
                    MediaPlayer.Pause();
                    soundOn = false;
                }
            }
        }

        // Draws Options menu
        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            main = DrawButton(spriteBatch, main, Point.Zero, font, "Main Menu");
            music = DrawButton(spriteBatch, music, new Point(-200, 0), font, "Music");
            video = DrawButton(spriteBatch, video, new Point(200, 0), font, "Window");
            back = DrawButton(spriteBatch, new Point(80, 40), Point.Zero, font, "Back");
        }
    }
}
