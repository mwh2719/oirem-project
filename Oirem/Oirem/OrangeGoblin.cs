﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Oirem
{
    /// <summary>
    /// Enemy that jumps and shoots
    /// </summary>
    class OrangeGoblin : EnemyBase
    {
        private double timer = 0.0;

        // Constructor
        public OrangeGoblin(int xLoc, int yLoc) : base(xLoc, yLoc)
        {
            intialY = yLoc;
            originalWidth = 283;
            originalHeight = 276;
            totalFrames = 42;
            timeToJump = 1.5;
            enemyRectangle = new Rectangle(xLoc, yLoc, 50, 50);
            jumper = true;
        }

        public override void Update(GraphicsDevice g1, GameTime gameTime, List<Tile> map)
        {
            

            timer += gameTime.ElapsedGameTime.TotalSeconds;
            if (isJumping == false)
            {
                if (timer >= timeToJump)
                {
                    timer = 0.0;
                    enemyRectangle.Y -= 30;
                    velocity.Y -= 15;
                    isJumping = true;
                    onGround = false;
                }
            }

            
            currentFrame++;
            base.Update(g1, gameTime, map);
        }
    }
}
