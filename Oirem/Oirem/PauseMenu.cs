﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Oirem
{
    /// <summary>
    /// Pauses game
    /// </summary>
    class PauseMenu : MenuManager
    {
        // Attributes
        private Rectangle resume;
        private Rectangle options;
        private Rectangle quit;
        private Rectangle main;
        private Rectangle underlay;

        // Properties
        public Rectangle Resume { get { return resume; } set { resume = value; } }
        public Rectangle Options { get { return options; } }
        public Rectangle Quit { get { return quit; } }
        public Rectangle Main { get { return main; } }

        //Constructor
        public PauseMenu(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window, Camera2D camera) : base(button, overlay, screenCtr, window, camera)
        {
            Point point = new Point(rectangle.Width + 15, rectangle.Height * 5);
            underlay = new Rectangle(rectangle.Location, point);
        }

        // Draws the Pause Menu
        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            //spriteBatch.Draw(overlay, underlay = new Rectangle(CenterOnScreen(underlay) + new Point(-1, 25), underlay.Size), Color.White);
            resume = DrawButton(spriteBatch, resume, new Point(0, -50), font, "Resume");
            options = DrawButton(spriteBatch, options, Point.Zero, font, "Options");
            main = DrawButton(spriteBatch, main, new Point(0, 50), font, "Main Menu");
            quit = DrawButton(spriteBatch, quit, new Point(0, 100), font, "Quit");
        }
    }
}
