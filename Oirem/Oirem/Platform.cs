﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class Platform
    {
        //attribute of platform size
        Rectangle platformRectangle;
        Texture2D platformSkin;

        //constuctor for platform
        public Platform(int x, int y, int width, int height, Texture2D sprite)
        {
            platformRectangle = new Rectangle(x, y, width, height);
            platformSkin = sprite;
        }

        //property for getting platform rectangle
        public Rectangle PlatformRectangle
        {
            get { return platformRectangle; }
            set { platformRectangle = value; }
        }
        public int PosX
        {
            get { return platformRectangle.X; }
            set { platformRectangle.X = value; }
        }
        public int PosY
        {
            get { return platformRectangle.Y; }
            set { platformRectangle.Y = value; }
        }

        //property for texture 
        public Texture2D PlatformSkin
        {
            get { return platformSkin; }
        }

        //checking for collison between platform and player
        public void CheckCollsion(Rectangle r1)
        {
            if (platformRectangle.Intersects(r1))
            {
                r1.Y = (r1.Height + platformRectangle.Y);
            }
        }

    }
}
