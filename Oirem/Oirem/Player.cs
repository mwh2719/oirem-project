﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Oirem
{
    public class Player
    {
        #region Attributes
        //Base attributes
        int playerHealth;
        int playerDamage;
        Texture2D spriteSheet;
        Rectangle current;
        Rectangle original;
        List<Rectangle> idle;
        List<Rectangle> walking;
        List<Rectangle> jumping;
        List<Rectangle> melee;
        List<Rectangle> ranged;
        Vector2 velocity;
        Vector2 landing;
        bool isJumping;
        int currentFrame;
        int totalFrames;
        int framesElapsed;
        double startTime;
        double timePerFrame = 150;
        const int WIDTH = 46;
        const int HEIGHT = 69;
        int distanceTraveled;
        bool isAttacking;

        // Tells which direction the player is facing
        bool faceLeft;
        bool faceRight;
        Spawner playerAttacks;
        bool usableMelee;
        bool usableRanged;

        // Rectangles for hitboxes
        Rectangle playerLeft;
        Rectangle playerRight;
        bool facingLeft;
        Projectile p1 = new Projectile();
        double timer = 0;
        double rangeAnimTime = 450.0;

        enum Anim
        {
            Idle,
            WalkLeft,
            WalkRight,
            Jumping,
            Melee,
            Ranged
        }

        enum Direction
        {
            Left,
            Right
        }

        Anim animation;
        Direction direction;
        #endregion

        #region Constructor
        public Player(Texture2D sprites)
        {
            spriteSheet = sprites;
            original = new Rectangle(250, 300, WIDTH, HEIGHT); // Rectangle to display character
            current = new Rectangle(original.X, original.Y, original.Width, HEIGHT); // Keeps track of player position
            idle = new List<Rectangle>(); 
            walking = new List<Rectangle>(); 
            jumping = new List<Rectangle>();
            melee = new List<Rectangle>();
            ranged = new List<Rectangle>();
            velocity = Vector2.Zero;
            landing = original.Location.ToVector2();
            isJumping = false;
            totalFrames = 1;
            currentFrame = 1;
            playerHealth = 3;
            distanceTraveled = 0;
            playerAttacks = new Spawner();
            faceLeft = false;
            faceRight = true;
            playerLeft = new Rectangle(PlayerRectangle.Left, PlayerRectangle.Y, 1, 1);
            playerRight = new Rectangle(PlayerRectangle.Right, PlayerRectangle.Y, 1, 1);

            if(playerAttacks.UsableAttacks()[0] == true)
            {
                usableMelee = true;
            }
            else
            {
                usableMelee = false;
            }

            if (playerAttacks.UsableAttacks()[1] == true)
            {
                usableRanged = true;
            }
            else
            {
                usableRanged = false;
            }

            // Adding the frames for each animtion
            // Jumping
            for (int i = 0; i < 3; i++)
            {
                idle.Add(new Rectangle(70 + (76 * i), 17, WIDTH, HEIGHT));
                jumping.Add(new Rectangle(42 + (70 * i), 201, WIDTH, 63));
                if (i != 0)
                    jumping[i] = new Rectangle(102 + (70 * i - 1), 189 + (2 * i - 1), WIDTH - 2, HEIGHT + 9);
            }
            // Walking and Melee
            for (int i = 0; i < 4; i++)
            {
                walking.Add(new Rectangle(47 + (80 * i), 107, WIDTH, HEIGHT));
                melee.Add(new Rectangle(246, 197, WIDTH, HEIGHT));
                if (i == 1)
                    melee[i] = new Rectangle(305, 197, WIDTH + 8, HEIGHT);
                if (i == 2)
                    melee[i] = new Rectangle(362, 197, WIDTH + 28, HEIGHT);
                if (i == 3)
                    melee[i] = new Rectangle(444, 198, WIDTH + 31, HEIGHT);
            }
            // Ranged
            for (int i = 0; i < 6; i++)
            {
                ranged.Add(new Rectangle(22, 550, 46, 69));
                if (i == 1)
                    ranged[1] = new Rectangle(88, 549, 49, 70);
                if (i == 2)
                    ranged[2] = new Rectangle(158, 548, 59, 71);
                if (i == 3)
                    ranged[3] = new Rectangle(231, 549, 56, 70);
                if (i == 4)
                    ranged[4] = new Rectangle(304, 551, 49, 68);
                if (i == 5)
                    ranged[5] = new Rectangle(366, 550, 46, 69);
            }
            animation = Anim.Idle; // Starts idle
            direction = Direction.Right; // Starts facing right
        }
        #endregion

        #region Player Properties
        //Properties for base attributes
        public Projectile P1
        {
            get
            {
                return p1;
            }
        }

        public int PlayerHealth
        {
            get
            {
                return playerHealth;
            }
            set
            {
                playerHealth = value;
            }
        }
        public int PosX
        {
            get
            {
                return original.X;
            }
            set
            {
                original.X = value;
            }
        }
        public int PosY
        {
            get
            {
                return original.Y;
            }
            set
            {
                original.Y = value;
            }
        }
        public int PlayerDamage
        {
            get
            {
                return playerDamage;
            }
            set
            {
                playerDamage = value;
            }
        }
        public Rectangle PlayerRectangle
        {
            get
            {
                return original;
            }
            set
            {
                original = value;
            }
        }
        public Rectangle Current
        {
            get { return current; }
            set { current = value; }
        }
        public int DistanceTraveled
        {
            get { return distanceTraveled; }
            set { distanceTraveled = value; }
        }
        public bool IsJumping
        {
            get { return isJumping; }
            set { isJumping = value; }
        }
        public bool IsAttacking
        {
            get { return isAttacking; }
        }
        public bool FaceRight
        {
            get { return faceRight; }
        }
        public bool FaceLeft
        {
            get { return faceLeft; }
        }
        public Rectangle PlayerLeft
        {
            get { return playerLeft; }
            set { playerLeft = value; }
        }
        public Rectangle PlayerRight
        {
            get { return playerRight; }
            set { playerRight = value; }
        }




        #endregion

        #region Update Method
        public void Update(KeyboardState kbState, KeyboardState oldKbState, GameTime gameTime, GraphicsDevice g1, List<Tile> map)
        {
            Rectangle prev = original;
            currentFrame = 1;

            // Melee Attack
            if (kbState.IsKeyDown(Keys.J))
            {
                if (usableMelee == true)
                {
                    SwitchAnim(Anim.Melee, gameTime);
                    totalFrames = melee.Count;
                    animation = Anim.Melee;
                    isAttacking = true;
                }
            }
            // Ranged attack
            else if (kbState.IsKeyDown(Keys.K))
            {
                if (p1.ProjectileActive == false)
                {
                    if (usableRanged == true)
                    {
                        SwitchAnim(Anim.Ranged, gameTime);
                        totalFrames = ranged.Count;
                        animation = Anim.Ranged;
                        timer += gameTime.ElapsedGameTime.Milliseconds;

                        if (timer >= rangeAnimTime)
                        {
                            p1 = new Projectile(facingLeft, PlayerRectangle);
                            timer = 0.0;
                        }
                    }
                }
            }
            // Move player left
            else if (kbState.IsKeyDown(Keys.A))
            {
                SwitchAnim(Anim.WalkLeft, gameTime);
                direction = Direction.Left;
                totalFrames = walking.Count;
                animation = Anim.WalkLeft;
                PosX -= 2;
                distanceTraveled+=2;
                facingLeft = true;
            }
            // Moves PLayer Right
            else if (kbState.IsKeyDown(Keys.D))
            {
                SwitchAnim(Anim.WalkRight, gameTime);
                direction = Direction.Right;
                totalFrames = walking.Count;
                animation = Anim.WalkRight;
                PosX += 2;
                distanceTraveled-=2;
                facingLeft = false;
            }
            // Plays idle animation when nothing is being pressed
            else
            {
                SwitchAnim(Anim.Idle, gameTime);
                totalFrames = idle.Count;
                animation = Anim.Idle;
            }


            if (kbState.IsKeyUp(Keys.J))
            {
                isAttacking = false;
            }
            if (!kbState.IsKeyDown(Keys.K))
            {
                timer = 0.0;
            }

            if(p1 != null)
            {
                p1.ProjectileMovement(g1);
                foreach(Tile t in map)
                {
                    t.CheckProjCollision(p1);
                }
            }

            // Updates character animation
            framesElapsed = (int)((gameTime.TotalGameTime.TotalMilliseconds - startTime) / timePerFrame);
            currentFrame = framesElapsed % totalFrames + 1;

            // Jumping logic
            original.Location += velocity.ToPoint(); // Moves the player
            current.Location = original.Location + velocity.ToPoint(); 

            // Checks collision with map
            bool onGround = false; // Checks whether or not the player is in the air
            foreach (Tile t in map)
            {
                // If player touches tile
                if (t.CheckCollision(original, prev))
                {
                    // If tile is a ground tile
                    if (t.Ground || (t.Platform && t.Active))
                    {
                        onGround = true;
                        original.Y = t.CurrentTile.Y - original.Height + 1;
                    }
                    // If tile is a wall tile
                    if (t.Wall && !t.Ground)
                    {
                        if (t.RightWall)
                        {
                            original.X = t.CurrentTile.X + t.CurrentTile.Width;
                        }
                        else
                        {
                            original.X = t.CurrentTile.X - original.Width;
                        }
                    }
                }
            }
            if (onGround)
            {
                isJumping = false;
            }
            else
            {
                isJumping = true;
            }

            //original.Location = current.Location; // Updates character based on interaction with tile

            // Jumping logic
            // When not jumping
            if (!isJumping)
            {
                velocity.Y = 0; // There is no upwards velocity

                // Jumps when jump button is pressed
                if (kbState.IsKeyDown(Keys.W) && oldKbState.IsKeyUp(Keys.W))
                {
                    currentFrame = 2;
                    totalFrames = jumping.Count;
                    animation = Anim.Jumping;
                    original.Y -= 50; // Height that wil be jumped to
                    velocity.Y = -15; // Downwards velocity
                    isJumping = true;
                }
            }
            // When jumping
            else
            {
                // Gravity acting on player
                int i = 2;
                velocity.Y += .4f * i;

                totalFrames = jumping.Count;
                animation = Anim.Jumping;
                currentFrame = 2;
            }

            // Facing right
            if(direction == Direction.Right)
            {
                faceRight = true;
                faceLeft = false;
            }
            // Facing left
            else if(direction == Direction.Left)
            {
                faceLeft = true;
                faceRight = false;
            }

        }

        private void SwitchAnim(Anim anim, GameTime gameTime)
        {
            if (animation != anim)
            {
                startTime = gameTime.TotalGameTime.TotalMilliseconds;
            }
        }
        #endregion

        #region Draw Method
        public void Draw(SpriteBatch spriteBatch)
        {
            currentFrame = currentFrame - 1; // Current animation frame
            switch (direction)
            {
                // When player is facing right
                case Direction.Right:
                    // Plays animation depending on player input
                    switch (animation)
                    {
                        case Anim.Idle:
                            spriteBatch.Draw(spriteSheet, original, idle[currentFrame], Color.White);
                            break;
                        case Anim.Jumping:
                            spriteBatch.Draw(spriteSheet, new Rectangle(original.X, original.Y, jumping[currentFrame].Width, jumping[currentFrame].Height), jumping[currentFrame], Color.White);
                            break;
                        case Anim.WalkLeft:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.WalkRight:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White);
                            break;
                        case Anim.Melee:
                            spriteBatch.Draw(spriteSheet, new Rectangle(original.X, original.Y, melee[currentFrame].Width, HEIGHT), melee[currentFrame], Color.White);
                            break;
                        case Anim.Ranged:
                            int displacedY = original.Y + (original.Height - ranged[currentFrame].Height);
                            spriteBatch.Draw(spriteSheet, new Rectangle(original.X, displacedY, ranged[currentFrame].Width, ranged[currentFrame].Height), ranged[currentFrame], Color.White);
                            break;
                    }
                    break;
                // When player is facing left
                case Direction.Left:
                    // Plays animation depending on player input
                    switch (animation)
                    {
                        case Anim.Idle:
                            spriteBatch.Draw(spriteSheet, original, idle[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.Jumping:
                            spriteBatch.Draw(spriteSheet, new Rectangle(original.X, original.Y, original.Width, jumping[currentFrame].Height), jumping[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.WalkLeft:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.WalkRight:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White);
                            break;
                        case Anim.Melee:
                            int displacedX = original.X + (original.Width - melee[currentFrame].Width);
                            spriteBatch.Draw(spriteSheet, new Rectangle(displacedX, original.Y, melee[currentFrame].Width, HEIGHT), melee[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.Ranged:
                            displacedX = original.X + (original.Width - ranged[currentFrame].Width);
                            int displacedY = original.Y + (original.Height - ranged[currentFrame].Height);
                            spriteBatch.Draw(spriteSheet, new Rectangle(displacedX, displacedY, ranged[currentFrame].Width, ranged[currentFrame].Height), ranged[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                    }
                    break;
            }

            if(p1 != null)
            {
                if (p1.MovingLeft == false)
                {
                    spriteBatch.Draw(spriteSheet, p1.ProjectileRectangle, new Rectangle(450, 580, 25, 25), Color.White);
                }
                else
                {
                    spriteBatch.Draw(spriteSheet, p1.ProjectileRectangle, new Rectangle(450, 580, 25, 25), Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                }
            }
        }
        #endregion
    }
}
