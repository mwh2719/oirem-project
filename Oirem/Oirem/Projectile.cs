﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oirem
{
    public class Projectile
    {
        // Attributes
        Rectangle projectileRectangle;
        bool projectileActive;
        bool movingLeft;

        //default constuructor
        public Projectile()
        {
            projectileRectangle = new Rectangle(-30, -30, 10, 10);
            projectileActive = false;
        }


        // Constructor
        public Projectile(bool direct, Rectangle playerRec)
        {
            movingLeft = direct;
            projectileActive = true;
            if(movingLeft == true)
            {
                projectileRectangle = new Rectangle(playerRec.X, (playerRec.Y + 25), 25, 25);
            }
            else
            {
                projectileRectangle = new Rectangle((playerRec.X + playerRec.Width), (playerRec.Y + 25), 25, 25);
            }
        }

        //constructor for enemy use
        public Projectile(Rectangle enemyRec)
        {
            projectileRectangle = new Rectangle((enemyRec.X + enemyRec.Width), (enemyRec.Y + 5), 25, 25);
            projectileActive = true;
            movingLeft = false;
        }

        public bool MovingLeft
        {
            get
            {
                return movingLeft;
            }
        }

        public Rectangle ProjectileRectangle
        {
            get
            {
                return projectileRectangle;
            }
        }

        public int PosX
        {
            get
            {
                return projectileRectangle.X;
            }
            set
            {
                projectileRectangle.X = value;
            }
        }

        public int PosY
        {
            get
            {
                return projectileRectangle.Y;
            }
            set
            {
                projectileRectangle.Y = value;
            }
        }

        public bool ProjectileActive
        {
            get
            {
                return projectileActive;
            }
            set
            {
                projectileActive = value;
            }
        }

        // Methods

        // Movement of the projectile
        public void ProjectileMovement(GraphicsDevice g1)
        {
            if(projectileActive == true)
            {
                if(movingLeft == true)
                {
                    PosX -= 5;
                }
                else
                {
                    PosX += 5;
                }
                if((PosX + ProjectileRectangle.Width) < 0 || PosX > g1.Viewport.Width)
                {
                    projectileActive = false;
                }
            }
            else
            {
                PosX = -30;
                PosY = -30;
            }
        }

    }
}
