﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Oirem
{
    class RedBee : EnemyBase
    {
        // Constructor
        public RedBee(int xLoc, int yLoc) : base(xLoc, yLoc)
        {
            intialY = yLoc;
            originalWidth = 573;
            originalHeight = 481;
            totalFrames = 2;
            y = 32;
            offset = 144;
            timePerFrame = 50;
            enemyRectangle = new Rectangle(xLoc, yLoc, 50, 50);
            bee = true;
        }

        // Update
        public override void Update(GraphicsDevice g1, GameTime gameTime, List<Tile> map)
        {
            framesElapsed = (int)(gameTime.TotalGameTime.TotalMilliseconds / timePerFrame);
            currentFrame = framesElapsed % totalFrames;
            onGround = true;
            base.Update(g1, gameTime, map);

        }
    }
}
