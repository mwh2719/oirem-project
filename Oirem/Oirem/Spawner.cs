﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.ViewportAdapters;

namespace Oirem
{
    /// <summary>
    /// Spawns enemies based on the "game.txt" file in the Contents folder
    /// </summary>
    public class Spawner
    {
        // Attributes
        private List<int> numSpawn;
        private Random rng = new Random();
        private int[,] mapSpawner;
        public List<int> NumSpawn { get { return numSpawn; } } 
        public int[,] MapSpawner { get { return mapSpawner; } }

        // Reads in the file and seperates by enemy type
        public Spawner()
        {
            StreamReader sr = new StreamReader(@"../../../../Content/game.txt");
            numSpawn = new List<int>();
            string[] line = sr.ReadToEnd().Split('\n');
            for (int i = 0; i < line.Length - 1; i++)
            {
                if (line[i].Contains(' '))
                {
                    int index = line[i].IndexOf(' ');
                    Int32.TryParse(line[i].Remove(0, index), out int result);
                    numSpawn.Add(result);
                }
            }
            mapSpawner = SpawnMap(sr, line);
            sr.Close();
        }

        
        //deciding what attacks the player can use
        public bool[] UsableAttacks()
        {
            //array of booleans to be returned
            bool[] attacks = new bool[2];
            StreamReader sr = new StreamReader(@"../../../../Content/game.txt");
            string[] line = sr.ReadToEnd().Split('\n');
            for (int i = 0; i < line.Length - 1; i++)
            {
                if (line[i].Contains('M'))
                {
                    if (line[i].Contains('T'))
                    {
                        attacks[0] = true;
                    }
                    else if (line[i].Contains('F'))
                    {
                        attacks[0] = false;
                    }
                }
                else if (line[i].Contains('R'))
                {
                    if(line[i].Contains('T'))
                    {
                        attacks[1] = true;
                    }
                    else if (line[i].Contains('F'))
                    {
                        attacks[1] = false;
                    }
                }
            }
            sr.Close();
            return attacks;
        }

        //postion of enemies
        int enemyType1 = 1200;
        double unrounded1;
        int prevEnemy1 = 0;
        int enemyType2 = 1500;
        int position1 = 0;
        int position2 = 0;
        int prevEnemy2 = 0;
        double unrounded2;
        double baseChange1 = 1;
        double baseChange2 = 1;
        int enemyType3 = 1800;
        double unrounded3;
        int prevEnemy3 = 0;
        int enemyType4 = 2000;
        int position3 = 0;
        int position4 = 0;
        int prevEnemy4 = 0;
        double unrounded4;
        double baseChange3 = 1;
        double baseChange4 = 1;
        Rectangle firstGroundTile;


        // Takes in the enemy type and spawns them
        // Seperated until second enemy is finished
        public List<EnemyBase> SpawnEnemies(List<Tile> map)
        {
            //saving the furthest left and bottom rectangle
            firstGroundTile = new Rectangle(1000, 1000, 10, 10);

            foreach(Tile t in map)
            {
                if(t.Platform == false)
                {
                    if (t.CurrentTile.Y < firstGroundTile.Y)
                    {
                        if (t.CurrentTile.X < firstGroundTile.X)
                        {
                            firstGroundTile = t.CurrentTile;
                        }
                    }
                }
            }


            List<EnemyBase> enemies = new List<EnemyBase>(); // List of enemies to be created
            for (int x = 0; x < numSpawn.Count; x++)
            {
                if (x == 1)
                {
                    for (int i = 0; i < (numSpawn[1]); i++)
                    {
                        position1 = prevEnemy1 - enemyType1;
                        enemies.Add(new GreenMonster(position1, firstGroundTile.Y - 50));
                        unrounded1 = enemyType1 / baseChange1;
                        enemyType1 = Convert.ToInt32(unrounded1);
                        prevEnemy1 = position1;
                        baseChange1 += 0.05;
                    }
                }

                if (x == 2)
                {
                    for (int i = 0; i < (numSpawn[2]); i++)
                    {
                        position2 = prevEnemy2 - enemyType2;
                        enemies.Add(new OrangeGoblin(position2, firstGroundTile.Y - 50));
                        unrounded2 = enemyType2 / baseChange2;
                        enemyType2 = Convert.ToInt32(unrounded2);
                        prevEnemy2 = position2;
                        baseChange2 += 0.1;
                    }
                }

                if (x == 3)
                {
                    for (int i = 0; i < (numSpawn[3]); i++)
                    {
                        position3 = prevEnemy3 - enemyType3;
                        enemies.Add(new LittleMonster(position3, firstGroundTile.Y - 50));
                        unrounded3 = enemyType3 / baseChange3;
                        enemyType3 = Convert.ToInt32(unrounded3);
                        prevEnemy3 = position3;
                        baseChange3 += 0.1;
                    }
                }

                if (x == 4)
                {
                    for (int i = 0; i < (numSpawn[4]); i++)
                    {
                        position4 = prevEnemy4 - enemyType4;
                        enemies.Add(new RedBee(position4, firstGroundTile.Y - 100));
                        unrounded4 = enemyType4 / baseChange4;
                        enemyType4 = Convert.ToInt32(unrounded4);
                        prevEnemy4 = position4;
                        baseChange4 += 0.1;
                    }
                }
            }
            return enemies;
        }

        // Creates map from game.txt
        public int[,] SpawnMap(StreamReader sr, string[] line)
        {
            string[] mapLines = new string[6]; // Saves the lines for Map
            int[,] tileNum = new int[6, 10]; // Numerical version of Map
            int index = 0; // Index for mapLines
            for (int i = 0; i < line.Length; i++)
            {
                if (!line[i].StartsWith("["))
                {
                    continue;
                }
                else
                {
                    mapLines[index] = line[i].Replace("[", ""); // Removes the first part of array of numbers
                    string[] nums = mapLines[index].Split(']'); // Splits the numbers up

                    // Adds them to numerical version of map
                    for (int j = 0; j < tileNum.GetLength(1); j++)
                    {
                        Int32.TryParse(nums[j], out tileNum[index, j]);
                    }
                    index++;
                }
            }
            return tileNum;
        }
    }
}
