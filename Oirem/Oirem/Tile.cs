﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Oirem
{
    public class Tile
    {
        // Attributes

        // If current tile is a platform, ground, or wall tile
        private bool platform = false; 
        private bool ground = false;
        private bool wall = false;
        private bool active = false;
        private bool cornerLeft = false;
        private bool cornerRight = false;

        // If wall, which way it faces
        private bool leftWall = false;
        private bool rightWall = false;

        private Texture2D texture; // Tile texture
        private Rectangle currentTile; 

        // Properties
        public bool Platform { get { return platform; } set { platform = value; } }
        public bool Ground { get { return ground; } set { ground = value; } }
        public bool Wall { get { return wall; } set { wall = value; } }
        public bool LeftWall { get { return leftWall; } }
        public bool RightWall { get { return rightWall; } }
        public bool Active { get { return active; } }
        public bool Corner { get { if (cornerLeft || cornerRight) { return true; } return false; } }
        public bool CornerLeft { get { return cornerLeft; } }
        public bool CornerRight { get { return cornerRight; } }
        public Rectangle CurrentTile { get { return currentTile; } set { currentTile = value; } }
        public Texture2D Texture { get { return texture; } set { texture = value; } }

        // Constructor
        public Tile(Texture2D tile, Rectangle rectangle, int tileNum)
        {
            TileType(tileNum); // Sets type of tile using info in game.txt
            texture = tile;
            currentTile = rectangle;
        }

        // Sets type of tile
        public void TileType(int num)
        {
            // Corner tiles
            if (num == 1 || num == 3)
            {
                if (num == 1)
                {
                    cornerLeft = true;
                }
                else
                {
                    cornerRight = true;
                }
                return;
            }
            // Ground tiles
            else if (num == 2 || num == 7 || num == 11)
            {
                ground = true;
                return;
            }
            // Walls
            else if (num == 4 || num == 6 || num == 12|| num == 16)
            {
                wall = true;
                if (num == 4 || num == 12)
                {
                    leftWall = true;
                }
                else
                {
                    rightWall = true;
                }
                return;
            }
            // Platform
            else if (num >= 13 && num <= 15)
            {
                platform = true;
                return;
            }
            return;
        }


        //check collison for projectile and map
        public void CheckProjCollision(Projectile proj)
        {
                if (LeftWall == true || RightWall == true || Ground == true || Wall == true || cornerRight == true || cornerLeft == true)
                {
                    if (proj.ProjectileRectangle.Intersects(CurrentTile))
                    {
                        proj.ProjectileActive = false;
                    }
                }
        }


        // Checks if another object is colliding with current tile
        public bool CheckCollision(Rectangle entity, Rectangle prev)
        {
            if (entity.Intersects(currentTile) && !platform)
            {
                if (cornerLeft || cornerRight)
                {
                    if (prev.Bottom <= currentTile.Top + 1)
                    {
                        ground = true;
                        wall = false;
                    }
                    else
                    {
                        ground = false;
                        if (cornerLeft)
                        {
                            wall = true;
                            leftWall = true;
                        }
                        else
                        {
                            wall = true;
                            rightWall = true;
                        }
                    }
                }
                //active = true;
                if (wall)
                {
                    if (leftWall)
                    {
                        if (entity.Right >= currentTile.Left)
                        {
                            return true;
                        }
                    }
                    else if (rightWall)
                    {
                        if (entity.Left <= currentTile.Right)
                        {
                            return true;
                        }
                    }
                }
                if (ground)
                {
                    if (entity.Bottom >= currentTile.Top)
                    {
                        return true;
                    }
                }
            }
            else if (platform)
            {
                if (prev.Bottom <= (currentTile.Top + 1))
                {
                    active = true;
                }
                else
                {
                    active = false;
                }

                if (active)
                {
                    if (entity.Intersects(currentTile))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
